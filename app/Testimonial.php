<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testimonial extends Model
{
    //
    use SoftDeletes;

    public function reservation()
    {
    	return $this->belongsTo('App\Reservaation');
    }
}

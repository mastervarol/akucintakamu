<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;

use Illuminate\Validation\Rule;
use Image;
use Session;
use Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Post::orderByDesc('created_at')->get();

        return view('admin.blog-index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.blog-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'title' => [
                'required',
                'max:75',
                Rule::unique('posts')->where( function($q){
                    $q->whereNull('created_at');
                })
            ],
            'short_desc' => 'required|max:225',
            'long_desc' => 'required',
            'image' => 'required'
        ]);

        $data = new Post;

        $data->title = ucwords($request->title);
        $data->short_desc = $request->short_desc;
        $data->long_desc = htmlentities($request->long_desc);
        $data->category = ucwords($request->category);

        if ($request->hasFile('image')) {
            $img = md5(str_random(64)). '.' .$request->file('image')->getClientOriginalExtension();

            $base_path = public_path().'/assets/img/blog/';

            $data->image = $img;

            Image::make($request->image)->save($base_path.$img);
        }

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'New post has created..');

        return redirect('admin/blog');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Post::find($id);

        return view('admin.blog-edit', [
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'title' => [
                'required',
                'max:75',
                Rule::unique('posts')->where( function($q){
                    $q->whereNull('created_at');
                })->ignore($id)
            ],
            'short_desc' => 'required|max:225',
            'long_desc' => 'required'
        ]);

        $data = Post::find($id);

        $data->title = ucwords($request->title);
        $data->short_desc = $request->short_desc;
        $data->long_desc = htmlentities($request->long_desc);
        $data->category = ucwords($request->category);

        if ($request->hasFile('image')) {
            $img = md5(str_random(64)). '.' .$request->file('image')->getClientOriginalExtension();

            $base_path = public_path().'/assets/img/blog/';

            $data->image = $img;

            Image::make($request->image)->save($base_path.$img);
        }

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'Post has edited..');

        return redirect('admin/blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Post::find($id)->delete();

        Session::flash('type', 'danger');
        Session::flash('icon', 'times');
        Session::flash('message', 'Post has deleted..');

        return redirect('admin/blog');
    }
}

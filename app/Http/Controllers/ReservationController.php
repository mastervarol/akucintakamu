<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\reservation;
use App\Option;
use App\Product;
use App\Guest;

use Illuminate\Validation\Rule;
use Image;
use Session;
use Validator;
use Carbon\Carbon;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Reservation::orderByDesc('created_at')->get();
        $product = Product::All();

        return view('admin.reservation-index', [
            'data' => $data,
            'product' => $product
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request->date;
        $validator = Validator::make($request->all(), [
            'contact_name' => [
                'required',
                'max:75'
            ],
            'product' => 'required',
            'date' => 'required|after:today',
            'contact_email' => 'required|email',
            'contact_phone' => 'required',
            'image' => 'required'
        ]);
        if ($validator->fails()) {
            Session::flash('error');

            return redirect('admin/reservation')
                ->withErrors($validator)
                ->withInput();
        }

        $productx = Product::find($request->product);
         $selected_date = $request->date;
         $guest_name = $request->contact_name;
         $guest_email = $request->contact_email;
         $guest_phone = $request->contact_phone;

         $guest_checked = Guest::where('name', $guest_name)
               ->where('email', $guest_email)
               ->first();

         if ($guest_checked) {
            $rsv = new Reservation;

            $rsv->product()->associate($productx->id);
            $rsv->guest()->associate($guest_checked->id);
            $rsv->code = str_random(7);
            $rsv->date = Carbon::parse($selected_date)->format('Y-m-d');
            $rsv->price = $productx->price;
            $rsv->status = 'BOOKED';

            if ($request->hasFile('image')) {
                $img = md5(str_random(64)). '.' .$request->file('image')->getClientOriginalExtension();

                $base_path = public_path().'/assets/img/chat/';

                $rsv->chat_proof = $img;

                Image::make($request->image)->save($base_path.$img);
            }

            $rsv->save();

            Session::flash('type', 'success');
            Session::flash('icon', 'check');
            Session::flash('message', 'Booking has made successfully');

            return redirect('admin/reservation/');
         }else{
            $guest = new Guest;

            $guest->name = ucwords($guest_name);
            $guest->email = $guest_email;
            $guest->phone = $guest_phone;

            $guest->save();

            $rsv = new Reservation;

            $rsv->product()->associate($productx->id);
            $rsv->guest()->associate($guest->id);
            $rsv->code = str_random(7);
            $rsv->date = Carbon::parse($selected_date)->format('Y-m-d');
            $rsv->price = $productx->price;
            $rsv->status = 'BOOKED';

            if ($request->hasFile('image')) {
                $img = md5(str_random(64)). '.' .$request->file('image')->getClientOriginalExtension();

                $base_path = public_path().'/assets/img/chat/';

                $rsv->chat_proof = $img;

                Image::make($request->image)->save($base_path.$img);
            }

            $rsv->save();

            Session::flash('type', 'success');
            Session::flash('icon', 'check');
            Session::flash('message', 'Booking has made successfully');

            return redirect('admin/reservation/');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Reservation::find($id);
        $option = Option::find(1);
        // return json_encode($data);
        return view('admin.reservation-detail', [
            'data' => $data,
            'option' => $option
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function confirm($id)
    {
        //
        $data = Reservation::find($id);

        $data->status = 'CONFIRMED';

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'Reservation has confirmed..');

        return redirect('admin/reservation/'. $id);
    }

    public function complete(Request $request, $id)
    {
        //
        $data = Reservation::find($id);

        $data->status = 'COMPLETED';

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'Reservation mark as completed..');

        return redirect('admin/reservation/'. $id);
    }

    public function pay(Request $request, $id)
    {
      // return json_encode($request->all());
        //
        $validator = Validator::make($request->all(), [
            'image_payment' => 'required|image',
            'image_chat.*' => 'required|image'
        ]);
        if ($validator->fails()) {
            Session::flash('error');

            return redirect('admin/reservation/'. $id)
                ->withErrors($validator)
                ->withInput();
        }

        $data = Reservation::find($id);

        $data->status = 'PAID';

        if ($request->hasFile('image_payment')) {
            $img = md5(str_random(64)). '.' .$request->file('image_payment')->getClientOriginalExtension();

            $base_path = public_path().'/assets/img/payment/';

            $data->payment_proof = $img;

            Image::make($request->image_payment)->save($base_path.$img);
        }

        if ($request->hasFile('image_chat')) {
          $img = array();
          $base_path = public_path().'/assets/img/chat/';

          foreach ($request->file('image_chat') as $img_chat) {
            $x = md5(str_random(64)). '.' .$img_chat->getClientOriginalExtension();
            array_push($img, $x);

            Image::make($img_chat)->save($base_path.$x);
          }
          $data->chat_proof = $img;
        }

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'Reservation confirmed and mark as paid..');

        return redirect('admin/reservation/'. $id);
    }

    public function print($id)
    {
        //
        $data = Reservation::find($id);
        $option = Option::find(1);

        return view('admin.reservation-print', [
            'data' => $data,
            'option' => $option
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gallery;

use Illuminate\Validation\Rule;
use Auth;
use Carbon\Carbon;
use Image;
use Session;
use Validator;
use Illuminate\Http\Response;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Gallery::orderBy('name')->paginate('12');

        return view('admin.gallery-index', [
            'data' => $data
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                'max:75',
                Rule::unique('galleries')->where( function($q){
                    $q->whereNull('deleted_at');
                })
            ],
            'description' => 'required',
            'category' => 'required|max:75',
            'image' => 'required|image'
        ]);
        if ($validator->fails()) {
            Session::flash('error');

            return redirect('admin/gallery')
                ->withErrors($validator)
                ->withInput();
        }

        $data = new Gallery;

        $data->name = ucwords($request->name);
        $data->description = $request->description;
        $data->category = ucwords($request->category);
        $data->link = $request->link;
        if (isset($request->date)) {
            $data->date = Carbon::parse($request->date)->format('Y-m-d');
        }else{
            $data->date = Carbon::now()->format('Y-m-d');
        }

        if ($request->hasFile('image')) {
            $img = md5(str_random(64)). '.' .$request->file('image')->getClientOriginalExtension();

            $base_path = public_path().'/assets/img/gallery/';

            $data->image = $img;

            Image::make($request->image)->save($base_path.$img);
        }

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'New image has uploaded..');

        return redirect('admin/gallery');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Gallery::find($id)->delete();

        Session::flash('type', 'danger');
        Session::flash('icon', 'times');
        Session::flash('message', 'Image was deleted..');

        return redirect('admin/gallery');
    }
}

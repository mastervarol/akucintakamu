<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Option;
use Session;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Option::find(1);

        return view('admin.option', [
            'data' => $data
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = Option::find(1);
        $tab = $request->tab;

        if ($tab == 1) {
            $data->about = $request->about;
            $data->site_title = $request->site_title;
            $data->site_tagline = $request->site_tagline;
        }
        if ($tab == 2) {
            $data->facebook_link = $request->facebook_link;
            $data->twitter_link = $request->twitter_link;
            $data->instagram_link = $request->instagram_link;
            $data->phone = $request->phone;
            $data->email = $request->email;
            $data->address = $request->address;
        }
        if ($tab == 3) {
            $data->contact_text = $request->contact_text;
            $data->term_condition = $request->term_condition;
        }
        if ($tab == 4) {
            $data->whatsapp = $request->whatsapp;
            $data->whatsapp_text = $request->whatsapp_text;
        }

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'Option updated..');

        return redirect('admin/option');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Illuminate\Validation\Rule;
use Session;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = User::All();

        return view('admin.user-index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.user-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:75',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->where(function($q){
                    $q->whereNull('deleted_at');
                })
            ],
            'password' => 'required',
            'password_confirmation' => 'same:password|required',
            'role' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('admin/user/create')
                ->withErrors($validator)
                ->withInput();
        }

        $data = new User;

        $data->name = strtoupper($request->name);
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->role = $request->role;

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'Data has stored..');

        return redirect('admin/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = User::find($id);

        return view('admin.user-edit', [
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:75',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->where(function($q){
                    $q->whereNull('deleted_at');
                })->ignore($id)
            ],
            'role' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('admin/user/'. $id .'/edit')
                ->withErrors($validator)
                ->withInput();
        }

        $data = User::find($id);

        $data->name = strtoupper($request->name);
        $data->email = $request->email;
        $data->role = $request->role;

        if (isset($request->password) || isset($request->password_confirmation)) {
            if (isset($request->password) == isset($request->password_confirmation)) {
                $data->password = bcrypt($request->password);
            }else{
                Session::flash('type', 'danger');
                Session::flash('icon', 'times');
                Session::flash('message', 'Passwor did not match');

                return redirect('admin/user/'. $id .'/edit')->withInput();
            }
        }

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'Data has updated..');

        return redirect('admin/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::find($id)->delete();

        Session::flash('type', 'danger');
        Session::flash('icon', 'times');
        Session::flash('message', 'Data has deleted..');

        return redirect('admin/user');
    }
}

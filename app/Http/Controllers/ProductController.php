<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

use Illuminate\Validation\Rule;
use Image;
use Session;
use Validator;
use Carbon\Carbon;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Product::orderBy('name')->get();

        return view('admin.product-index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.product-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'image' => 'required',
            'name' => [
                'required',
                Rule::unique('products')->where(function($q){
                    $q->whereNull('deleted_at');
                })
            ],
            'desc' => 'required',
            'price' => 'required',
            'unit' => 'required',
            'is_promo' => 'required',
            'category' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('admin/product/create')
                ->withErrors($validator)
                ->withInput();
        }

        $data = new Product;

        $data->name = ucwords($request->name);
        $data->description = $request->desc;
        $data->price = $request->price;
        $data->unit = ucwords($request->unit);
        $data->is_promo = $request->is_promo;
        $data->category = ucwords($request->category);
        $data->additional_info = $request->additional_info;

        if ($request->hasFile('image')) {
            $img = md5(str_random(64)). '.' .$request->file('image')->getClientOriginalExtension();

            $base_path = public_path().'/assets/img/product/';

            $data->image = $img;

            Image::make($request->image)->save($base_path.$img);
        }

        if ($request->is_promo == 1) {
            $data->promo_price = $request->promo_price;
            $data->promo_start = Carbon::parse($request->promo_start)->format('Y-m-d');
            $data->promo_end = Carbon::parse($request->promo_end)->format('Y-m-d');
            
            if ($request->hasFile('promo_image')) {
                $img = md5(str_random(64)). '.' .$request->file('promo_image')->getClientOriginalExtension();

                $base_path = public_path().'/assets/img/product/';

                $data->image = $img;

                Image::make($request->image)->save($base_path.$img);
            }
        }

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'Data has stored..');

        return redirect('admin/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Product::find($id);

        return view('admin.product-edit', [
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('products')->where(function($q){
                    $q->whereNull('deleted_at');
                })->ignore($id)
            ],
            'desc' => 'required',
            'price' => 'required',
            'unit' => 'required',
            'is_promo' => 'required',
            'category' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('admin/product/create')
                ->withErrors($validator)
                ->withInput();
        }

        $data = Product::find($id);

        $data->name = ucwords($request->name);
        $data->description = $request->desc;
        $data->price = $request->price;
        $data->unit = ucwords($request->unit);
        $data->is_promo = $request->is_promo;
        $data->category = ucwords($request->category);
        $data->additional_info = $request->additional_info;

        if ($request->hasFile('image')) {
            $img = md5(str_random(64)). '.' .$request->file('image')->getClientOriginalExtension();

            $base_path = public_path().'/assets/img/product/';

            $data->image = $img;

            Image::make($request->image)->save($base_path.$img);
        }

        if ($request->is_promo == 1) {
            $data->promo_price = $request->promo_price;
            $data->promo_start = Carbon::parse($request->promo_start)->format('Y-m-d');
            $data->promo_end = Carbon::parse($request->promo_end)->format('Y-m-d');
            
            if ($request->hasFile('promo_image')) {
                $img = md5(str_random(64)). '.' .$request->file('promo_image')->getClientOriginalExtension();

                $base_path = public_path().'/assets/img/product/';

                $data->image = $img;

                Image::make($request->image)->save($base_path.$img);
            }
        }

        $data->save();

        Session::flash('type', 'success');
        Session::flash('icon', 'check');
        Session::flash('message', 'Data has updated..');

        return redirect('admin/product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Product::find($id)->delete();

        Session::flash('type', 'danger');
        Session::flash('icon', 'times');
        Session::flash('message', 'Data has deleted..');

        return redirect('admin/product');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $casts = [
      'chat_proof' => 'array'
    ];

    //
    public function guest()
    {
    	return $this->belongsTo('App\Guest')->withTrashed();
    }

    public function product()
    {
    	return $this->belongsTo('App\Product')->withTrashed();
    }

    public function testimonial()
    {
    	return $this->belongsTo('App\Testimonial')->withTrashed();
    }
}

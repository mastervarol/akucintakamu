@extends('template')
@push('css')
	{{-- expr --}}
@endpush
@section('content')

<div id="main-content" class="edge-padding">
	<!-- Contact page main -->
	<section class="page-section contact-page">
		<div class="container">
			<!-- contact page text + image -->
			<div class="row contact-page-text">
				<div class="col-xlarge-5 col-medium-6">
					<h1 class="font-reg">Contact</h1>
					<div class="page-content">
						<p>{{ $option->about }}</p>
					</div>
				</div>
				<div class="col-xlarge-7 col-medium-6">
					<div class="contact-image">
						<img src="assets/img/contact_placeholder.jpg" alt="contact image" class="image" />
					</div>
				</div>
			</div>
			@if (Session::has('message'))
				<div id="success-message" class="notification" style="display: block;"><p class="font-reg"></p>{{ Session::get('message') }}</div>
			@endif
			<div id="error-message" class="notification"><p class="font-reg"></p>message</div>
			<!-- contact form -->
			{!! Form::open(['url' => url('contact-us/send'), 'role' => 'form', 'method' => 'POST']) !!}
			{{-- <form id="contact-form" method="post" url="{{ url('contact-us/send') }}" name="contact-form"> --}}
				<div class="row">
					<div class="col-xlarge-4 col-medium-4">
						<input type="text" class="contact-input font-reg" name="contact_name" id="contact_name" value="" placeholder="Your Name" tabindex="1" required/>
					</div>
					<div class="col-xlarge-4 col-medium-4">
						<input type="email" class="contact-input font-reg" name="contact_email" id="contact_email" value="" placeholder="Your Email" tabindex="2"  required/>
					</div>
					<div class="col-xlarge-4 col-medium-4">
						<input type="text" class="contact-input font-reg" name="contact_subject" id="contact_subject" value="" placeholder="Your Subject" tabindex="3"  required/>
					</div>
				</div>
				<div class="row">	
					<div class="col-xlarge-12">
						<textarea name="contact_message" id="contact_message" class="contact-text font-reg" tabindex="4" placeholder="Your Message" style="resize: none;" required></textarea>
					</div>
				</div>
				<input type="submit" class="primary-button font-reg hov-bk" value="Submit" />
			{!! Form::close() !!}
		</div>
	</section>
</div>

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
@endpush
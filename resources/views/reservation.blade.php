@extends('template')
@push('css')
	{{-- expr --}}
	<style type="text/css">
		@import url(https://fonts.googleapis.com/css?family=Lato:700);
	.box {
	  position: relative;
	  max-width: 600px;
	  width: 90%;
	  height: 400px;
	  background: #fff;
	  box-shadow: 0 0 15px rgba(0,0,0,.1);
	}

	/* common */
	.ribbon {
	  width: 150px;
	  height: 150px;
	  overflow: hidden;
	  position: absolute;
	}
	.ribbon::before,
	.ribbon::after {
	  position: absolute;
	  z-index: -1;
	  content: '';
	  display: block;
	  border: 5px solid #2980b9;
	}
	.ribbon span {
	  position: absolute;
	  display: block;
	  width: 225px;
	  padding: 15px 0;
	  background-color: #111111;
	  box-shadow: 0 5px 10px rgba(0,0,0,.1);
	  color: #fff;
	  font: 700 18px/1 'Lato', sans-serif;
	  text-shadow: 0 1px 1px rgba(0,0,0,.2);
	  text-transform: uppercase;
	  text-align: center;
	  opacity: 0.8;
	}

	/* top left*/
	.ribbon-top-left {
	  top: 0px;
	  /*left: -10px;*/
	}
	.ribbon-top-left::before,
	.ribbon-top-left::after {
	  border-top-color: transparent;
	  border-left-color: transparent;
	}
	.ribbon-top-left::before {
	  top: 0;
	  right: 0;
	}
	.ribbon-top-left::after {
	  bottom: 0;
	  left: 0;
	}
	.ribbon-top-left span {
	  right: -25px;
	  top: 30px;
	  transform: rotate(-45deg);
	}

	/* top right*/
	.ribbon-top-right {
	  top: -10px;
	  right: -10px;
	}
	.ribbon-top-right::before,
	.ribbon-top-right::after {
	  border-top-color: transparent;
	  border-right-color: transparent;
	}
	.ribbon-top-right::before {
	  top: 0;
	  left: 0;
	}
	.ribbon-top-right::after {
	  bottom: 0;
	  right: 0;
	}
	.ribbon-top-right span {
	  left: -25px;
	  top: 30px;
	  transform: rotate(45deg);
	}

	/* bottom left*/
	.ribbon-bottom-left {
	  bottom: -10px;
	  left: -10px;
	}
	.ribbon-bottom-left::before,
	.ribbon-bottom-left::after {
	  border-bottom-color: transparent;
	  border-left-color: transparent;
	}
	.ribbon-bottom-left::before {
	  bottom: 0;
	  right: 0;
	}
	.ribbon-bottom-left::after {
	  top: 0;
	  left: 0;
	}
	.ribbon-bottom-left span {
	  right: -25px;
	  bottom: 30px;
	  transform: rotate(225deg);
	}

	/* bottom right*/
	.ribbon-bottom-right {
	  bottom: -10px;
	  right: -10px;
	}
	.ribbon-bottom-right::before,
	.ribbon-bottom-right::after {
	  border-bottom-color: transparent;
	  border-right-color: transparent;
	}
	.ribbon-bottom-right::before {
	  bottom: 0;
	  left: 0;
	}
	.ribbon-bottom-right::after {
	  top: 0;
	  right: 0;
	}
	.ribbon-bottom-right span {
	  left: -25px;
	  bottom: 30px;
	  transform: rotate(-225deg);
	}
	.page-link{
		border-color: #111111 !important;
		color: #111111 !important;
	}
	.pagination>.active>span {
		background-color: #111111 !important;
		color: #fff !important;
	}
	</style>
	
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
@endpush
@section('content')

<div id="main-content">
	<div class="feature-page">
		<!-- theme slideshows -->
		<section class="feature-slideshows feature-section">
			<div class="container">
				<!-- Section title -->
				<div class="section-title font-reg">
					<h2>Reservation </h2>
					<p>Create stunning slideshow to showcase images</p>
				</div>
				<div id="feature-slideshow-outer" class="carousel-outer">
					<!-- Previous slide button -->
					<span class="slideshow-btn previous-slide-btn fa fa-angle-left"></span>
					<!-- Single portfolio slideshow -->
					<div id="feature-slideshow" class="carousel">
						@foreach ($slide as $element)
							<!-- feature slide example -->
							<div class="feature-slide" style="background-image:url('{{ asset('assets/img/slide/'. $element->image) }}');">
							</div>
						@endforeach
					</div>
					<!-- Next slide button -->
					<span class="slideshow-btn next-slide-btn fa fa-angle-right"></span>
				</div>
				<div class="feature-slide-animation">
					<div class="row">
						<div class="col-xlarge-1 col-medium-1">
						</div>
						<div class="col-xlarge-8 col-medium-8">
						{!! Form::open(['url' => url('reservation'), 'role' => 'form', 'method' => 'GET']) !!}
							<input type="hidden" name="q" value="1">
							<div class="row">
								<div class="col-xlarge-4 col-medium-8">
								</div>
								<div class="col-xlarge-3 col-medium-6">
									<select id="slideshow-animation-in" class="slideshow-animation-select" name="product" required>
										<option value="" selected hidden>Select Product</option>
										@foreach ($product->where('category', '<>', 'Printed Stuff') as $element)
											<option value="{{ $element->id }}">{{ $element->name }}</option>
										@endforeach
										{{-- <optgroup label="In Promo">
										  @foreach ($product->where('is_promo', 1)->where('category', '<>', 'Printed Stuff') as $element)
												<option value="{{ $element->id }}">{{ $element->name }}</option>
											@endforeach
										</optgroup> --}}
									</select>
								</div>
								<div class="col-xlarge-3 col-medium-6">
									<input type="text" class="contact-input font-reg date-mask" name="date" id="date" value="" placeholder="Date" tabindex="1" required/>
								</div>
								<input type="hidden" name="q" value="1">
								<div class="col-xlarge-2 col-medium-3" style="text-align: center;">
									<input type="submit" class="primary-button font-reg hov-bk" value="Check" style="height: 50px;" />
								</div>
								<div class="col-xlarge-2 col-medium-4">
								</div>
							</div>
						{!! Form::close() !!}
						</div>
					</div>
				</div>
				@if(request()->q == 1)
					<section class="blog-section page-section" style="padding-top: 0px; !important;">
						<div class="container">
							<div class="row">
								<div class="col-xlarge-1 col-medium-1">
								</div>
								<div class="col-xlarge-9 col-medium-9">
									@if (Session::has('message'))
										<div id="error-message" class="notification" style="display: block; text-align: center; "><p class="font-reg"></p>{{ Session::get('message') }}</div>
									@else
										<div id="success-message" class="notification" style="display: block; text-align: center; "><p class="font-reg"></p>Product available on selected date</div>
									@endif
									<!-- Small blog item example -->
									<div class="blog-item">
										<div class="row">
											<div class="col-xlarge-5 col-medium-4">
												<a href="{{ url('product/'. $product_result->name) }}"><img src="{{ asset('assets/img/product/'. $product_result->image ) }}" alt="" class="image" style="height: 180px;" />
												@if ($product_result->is_promo == 1)
													<div class="ribbon ribbon-top-left"><span>PROMOOO!!!</span></div>
												@endif
												</a>
											</div>
											<div class="col-xlarge-7 col-medium-8">
												<h3 class="font-reg"><a href="">{{ $product_result->name }}</a></h3>
												<div class="blog-item-meta">
													<span>{{ \Carbon\Carbon::parse($product_result->created_at)->format('d M Y') }}</span>
												</div>
												<div class="page-content">
													<p>{{ $product_result->description }}</p>
												</div>
												<a href="{{ url('reservation/detail/'. $product_result->name .'?date='. request()->date) }}" class="primary-button font-reg hov-bk">Book Now</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xlarge-1 col-medium-1">
								</div>
							</div>
						</div>
					</section>
				@else
					@if(Session::has('message'))
						<div id="error-message" class="notification" style="display: block; text-align: center; "><p class="font-reg"></p>{{ Session::get('message') }}</div>
					@else
						<div class="page-content" style="text-align: center;">
						<h1 style="margin-top: 50px;">Check Availability</h1>
					</div>
					@endif
				@endif
			</div>
		</section>
	</div>

</div>

@endsection
@push('plugin')
	{{-- expr --}}
   <!-- InputMask -->
   <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.js') }}"></script>
   <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
   <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
@endpush
@push('script')
	{{-- expr --}}
	<script type="text/javascript">
		$(document).ready( function(){
			//Datemask dd/mm/yyyy
         $('.date-mask').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		});
	</script>
@endpush
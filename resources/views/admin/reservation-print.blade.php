<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>{{ $option->site_title }} | Invoice #{{ $data->code }}</title>
   <!-- Tell the browser to be responsive to screen width -->
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <!-- Bootstrap 3.3.7 -->
   <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
   <!-- Font Awesome -->
   <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
   <!-- Ionicons -->
   <link rel="stylesheet" href="{{ asset('assets/bower_components/Ionicons/css/ionicons.min.css') }}">
   <!-- Theme style -->
   <link rel="stylesheet" href="{{ asset('assets/dist/css/AdminLTE.min.css') }}">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="{{ url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic') }}">
</head>
<body onload="window.print();">
   <div class="wrapper">
      <!-- Main content -->
   <section class="invoice">
      <!-- title row -->
      <div class="row">
         <div class="col-xs-12">
            @if (Session::has('message'))
               {{-- Alert --}}
               <div class="alert alert-{{ Session::get('type') }} alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-{{ Session::get('icon') }}"></i> Success!</h4>
                  {{ Session::get('message') }}
               </div>
               {{-- Alert --}}
            @endif
            <h2 class="page-header">
               <i class="fa fa-globe"></i> {{ $option->site_title }}
               <small class="pull-right">Date: {{ \Carbon\Carbon::parse($data->created_at)->format('d/m/y') }}</small>
            </h2>
         </div>
         <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
         <div class="col-sm-4 invoice-col">
            From
            <address>
               <strong>{{ $option->site_title }}</strong><br>
               {{ $option->address }}<br>
               Phone: {{ $option->phone }}<br>
               Email: {{ $option->email }}
            </address>
         </div>
         <!-- /.col -->
         <div class="col-sm-4 invoice-col">
            To
            <address>
               <strong>{{ $data->guest->name }}</strong><br>
               Phone: {{ is_null($data->guest->phone) ? '-' : $data->guest->phone }}<br>
               Email: {{ is_null($data->guest->email) ? '-' : $data->guest->email }}
            </address>
         </div>
         <!-- /.col -->
         <div class="col-sm-4 invoice-col">
            <b>Invoice #{{ $data->code }}</b><br>
            <br>
            <b>Order ID:</b> 4F3S8J<br>
            <b>Payment Due:</b> 2/22/2014<br>
            <b>Account:</b> 968-34567
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
         <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
               <thead>
                  <tr>
                     <th>Qty</th>
                     <th>Product</th>
                     <th>Subtotal</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td>1</td>
                     <td>{{ $data->product->name }}</td>
                     <td>IDR {{ number_format($data->price, 0, ',', '.') }}</td>
                  </tr>
               </tbody>
            </table>
         </div>
         <!-- /.col -->
      </div>
   </section>
   <!-- /.content -->
   </div>
   <!-- ./wrapper -->
</body>
</html>

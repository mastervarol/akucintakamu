@extends('admin.template')
@push('css')
  {{-- expr --}}
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Reservation
        <small>list</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><a href="#"> Reservation</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row" style="display: none;">
        <div class="col-md-12">
          @if (Session::has('message'))
            {{-- Alert --}}
            <div class="alert alert-{{ Session::get('type') }} alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-{{ Session::get('icon') }}"></i> Success!</h4>
              {{ Session::get('message') }}
            </div>
            {{-- Alert --}}
          @endif
          <div class="box box-success {{ Session::has('error') ? '' : 'collapsed-box' }}">
            <div class="box-header with-border">
              <h3 class="box-title">Add new</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-2">

                </div>
                <div class="col-md-8">
                  @if($errors->any())
                    <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                  @endif
                  <!-- form start -->
                  {!! Form::open(['url' => url('admin/reservation/'), 'role' => 'form', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
                  <div class="box-body">
                    <div class="form-group">
                      <label for="title" class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-8">
                        <select class="form-control select2-hidden-accessible" name="product" id="product" requiredd>
                          <option value="" selected hidden>Product</option>
                          @foreach ($product as $element)
                            <option value="{{ $element->id }}">{{ $element->name }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="date" class="col-sm-2 control-label">Date:</label>
                      <div class="input-group col-sm-8" style="padding-left: 15px;">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask="" style="width: 449px;" id="date" name="date" required>
                      </div>
                      <!-- /.input group -->
                    </div>
                    <div class="form-group">
                      <label for="contact_name" class="col-sm-2 control-label">Guest Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="Name" value="{{ old('contact_name') }}" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="contact_email" class="col-sm-2 control-label">Guest Email</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="contact_email" name="contact_email" placeholder="Email" value="{{ old('contact_email') }}" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="contact_phone" class="col-sm-2 control-label">Guest Phone</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="contact_phone" name="contact_phone" placeholder="Phone" value="{{ old('contact_phone') }}" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="image" class="col-sm-2 control-label">Select Chat Proof</label>
                      <div class="col-sm-8">
                        <input type="file" id="image" name="image" onchange="previewImage();" required>
                      </div>
                    </div>
                    <div class="form-group" style="text-align: center; vertical-align: middle; display: none;" id="preview-container">
                      <label for="image" class="col-sm-2 control-label">Chat Proof Preview</label>
                      <div class="col-sm-8">
                        <img id="image-preview" src="{{ asset('assets/dist/img/blank.jpg') }}"  style="max-width: 400px; max-height: 300px;" alt="">
                      </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" data-widget="collapse">Cancel</button>
                    <button type="submit" class="btn btn-success pull-right">Submit</button>
                  </div>
                  <!-- /.box-footer -->
                  {!! Form::close() !!}
                </div>
              </div>
              <!-- /.row -->
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-success">
            <!-- /.box-header -->
            {{-- <div class="box-header">
            <a href="{{ url('admin/reservation/create') }}" class="btn btn-success" style="width: 50px; height: 25px;"><i class="fa fa-plus"></i></a>
          </div> --}}
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 150px; text-align: center;">Date (d/m/y)</th>
                  <th>Code</th>
                  <th>Guest</th>
                  <th>Product</th>
                  <th style="width: 7%;">#</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $d)
                  <tr>
                    <td style="text-align: center;">
                      Booking : {{ \Carbon\Carbon::parse($d->created_at)->format('d/m/y') }} <br>
                      <strong>Arrival : {{ \Carbon\Carbon::parse($d->date)->format('d/m/y') }}</strong>
                    </td>
                    <td>{{ $d->code }} - {{ $d->status }}</td>
                    <td>{{ $d->guest->name }}</td>
                    <td>{{ $d->product->name }}</td>
                    <td align="center">
                      <div class="btn-group">
                        <a href="{{ url('admin/reservation/'. $d->id) }}" class="btn btn-default"><i class="fa fa-eye"></i></a>
                        {{-- {!! Form::open(['url' => url('admin/reservation/'. $d->id), 'role' => 'form', 'method' => 'delete', 'class' => 'form-horizontal']) !!}
                        <a href="{{ url('admin/reservation/'. $d->id) }}" class="btn btn-default"><i class="fa fa-eye"></i></a>
                        <button type="submit" name="submit" class="btn btn-danger" onclick="return confirm('Delete data..?');"><i class="fa fa-trash"></i></button>
                        {!! Form::close() !!} --}}
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@push('plugin')
  {{-- expr --}}
  <!-- DataTables -->
  <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
  <!-- InputMask -->
  <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.js') }}"></script>
  <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
  <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
@endpush
@push('script')
  {{-- expr --}}
  <script>
  $(document).ready(function(){
    console.log('document ready');

    //Datemask dd/mm/yyyy
    $('#date').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });

    $('#reservation').addClass('active');
    $('#example1').DataTable({
      'sort': false
    });
  });

  function previewImage() {
    var reader = new FileReader();
    reader.readAsDataURL(document.getElementById("image").files[0]);

    reader.onload = function(readerEvent) {
      document.getElementById("image-preview").src = readerEvent.target.result;
    };

    $('#preview-container').css('display', 'block');
  };
  </script>
@endpush

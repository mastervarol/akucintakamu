@extends('admin.template')
@push('css')
	{{-- expr --}}
@endpush
@section('content')
	
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Gallery
         <small>list</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
         <li class="active"><a href="#"> Gallery</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-success {{ Session::has('error') ? '' : 'collapsed-box' }}">
               <div class="box-header with-border">
                  <h3 class="box-title">Add new</h3>

                  <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                     {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> --}}
                  </div>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <div class="row">
                     <div class="col-md-6" style="text-align: center; vertical-align: middle;">
                        <img id="image-preview" src="{{ asset('assets/dist/img/blank.jpg') }}"  style="max-width: 400px; max-height: 300px;" alt="">
                     </div>
                     <div class="col-md-6">
                        @if($errors->any())
                           <div class="alert alert-danger">
                              <ul>
                                 @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                 @endforeach
                              </ul>
                           </div>
                        @endif
                        <!-- form start -->
                        {!! Form::open(['url' => url('admin/gallery/'), 'role' => 'form', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
                           <div class="box-body">
                              <div class="form-group">
                                 <label for="name" class="col-sm-2 control-label">Name</label>
                                 <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name') }}" required>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="description" class="col-sm-2 control-label">Description</label>
                                 <div class="col-sm-10">
                                    <textarea class="form-control" rows="3" id="description" name="description" placeholder="Description here" style="resize: none;" required>{{ old('description') }}</textarea>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="name" class="col-sm-2 control-label">Category</label>
                                 <div class="col-sm-10">
                                    <input type="text" class="form-control" id="category" name="category" placeholder="e.g. Sea View" value="{{ old('category') }}" required>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="link" class="col-sm-2 control-label">Link</label>
                                 <div class="col-sm-10">
                                    <input type="text" class="form-control" id="link" name="link" placeholder="Link" value="{{ old('name') }}">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="date" class="col-sm-2 control-label">Date:</label>
                                 <div class="input-group col-sm-9" style="padding-left: 15px;">
                                    <div class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask="" style="width: 335px;" id="date" name="date">
                                 </div>
                                 <!-- /.input group -->
                              </div>
                              <div class="form-group">
                                 <label for="image" class="col-sm-2 control-label">Select Image</label>
                                 <div class="col-sm-10">
                                    <input type="file" id="image" name="image" onchange="previewImage();" required>
                                    <p class="help-block">Example block-level help text here.</p>
                                 </div>
                              </div>
                           </div>
                           <!-- /.box-body -->
                           <div class="box-footer">
                              <button type="button" class="btn btn-default" data-widget="collapse">Cancel</button>
                              <button type="submit" class="btn btn-success pull-right">Submit</button>
                           </div>
                           <!-- /.box-footer -->
                        {!! Form::close() !!}
                     </div>
                  </div>
                  <!-- /.row -->
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            @if (Session::has('message'))
               {{-- Alert --}}
               <div class="alert alert-{{ Session::get('type') }} alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-{{ Session::get('icon') }}"></i> Success!</h4>
                  {{ Session::get('message') }}
               </div>
               {{-- Alert --}}
            @endif
         </div>
         @foreach ($data as $element)
            <div class="col-md-3">
               <div class="box box-solid">
                  <div class="box-header with-border" style="text-align: center;">
                     <label>{{ $element->name }}</label> <br> <small>{{ $element->category }}</small>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                     <img src="{{ asset('assets/img/gallery/'. $element->image) }}"  style="max-width: 205px; max-height: 200px;" alt="">
                  </div>
                  <div class="box-footer" style="text-align: center;">
                     {!! Form::open(['url' => url('admin/gallery/'. $element->id), 'role' => 'form', 'method' => 'delete', 'class' => 'form-horizontal']) !!}
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Delete data..?');"><i class="fa fa-times"></i></button>
                     {!! Form::close() !!}
                  </div>
               </div>
               <!-- /.box -->
            </div>
            <!-- ./col -->
         @endforeach
      </div>
      <!-- /.row -->
      <div class="row" style="text-align: center;">
         {{ $data->links() }}
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@push('plugin')
	{{-- expr --}}
   <!-- InputMask -->
   <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.js') }}"></script>
   <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
   <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
@endpush
@push('script')
	{{-- expr --}}
   <script>
      $(document).ready(function(){
         console.log('document ready');

         $('#gallery').addClass('active');

         //Datemask dd/mm/yyyy
         $('#date').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
      });

      function previewImage() {
         var reader = new FileReader();
         reader.readAsDataURL(document.getElementById("image").files[0]);

         reader.onload = function(readerEvent) {
         document.getElementById("image-preview").src = readerEvent.target.result;
         };
      };
   </script>
@endpush
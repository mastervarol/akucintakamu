@extends('admin.template')
@push('css')
	{{-- expr --}}
@endpush
@section('content')
	
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Dashboard
         <small>it all starts here</small>
      </h1>
      <ol class="breadcrumb">
         <li class="active"><a href="{{ url('/admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Default box -->
      <div class="box box-success">
         <div class="box-header with-border">
            <h3 class="box-title">Howdy {{ Auth::user()->name }}, <small>welcome back..</small></h3>
         </div>
         <div class="box-body">
            Start creating your amazing application!
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
   <script>
      $(document).ready(function(){
         console.log('document ready');

         $('#dashboard').addClass('active');
      });
   </script>
@endpush
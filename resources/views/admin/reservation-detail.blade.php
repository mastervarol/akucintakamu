@extends('admin.template')
@push('css')
	{{-- expr --}}
@endpush
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Reservation
         <small>Detail</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('admin/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{ url('admin/reservation') }}">Reservation</a></li>
         <li class="active">Detail</li>
      </ol>
   </section>

   <div class="pad margin no-print">
      @if (Session::has('message'))
         {{-- Alert --}}
         <div class="alert alert-{{ Session::get('type') }} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-{{ Session::get('icon') }}"></i> Success!</h4>
            {{ Session::get('message') }}
         </div>
         {{-- Alert --}}
      @else
      <div class="callout callout-info" style="margin-bottom: 0!important;">
         <h4><i class="fa fa-info"></i> Note:</h4>
         @if ($data->status == 'BOOKED')
            Confirm this reservation to continue..
         @elseif ($data->status == 'CONFIRMED')
            Reservation confirmed..
         @elseif ($data->status == 'COMPLETED')
            Reservation completed..
         @else
            Reservation was paid..
         @endif
      </div>
      @endif
   </div>

   <!-- Main content -->
   <section class="invoice">
      <!-- title row -->
      <div class="row">
         <div class="col-xs-12">
            <h2 class="page-header">
               <i class="fa fa-globe"></i> {{ $option->site_title }}
               <small class="pull-right">Date: {{ \Carbon\Carbon::parse($data->created_at)->format('d/m/y') }}</small>
            </h2>
         </div>
         <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
         <div class="col-sm-3 invoice-col">
            From
            <address>
               <strong>{{ $option->site_title }}</strong><br>
               {{ $option->address }}<br>
               Phone: {{ $option->phone }}<br>
               Email: {{ $option->email }}
            </address>
         </div>
         <!-- /.col -->
         <div class="col-sm-3 invoice-col">
            To
            <address>
               <strong>{{ $data->guest->name }}</strong><br>
               Phone: {{ is_null($data->guest->phone) ? '-' : $data->guest->phone }}<br>
               Email: {{ is_null($data->guest->email) ? '-' : $data->guest->email }}
            </address>
         </div>
         <!-- /.col -->
         <div class="col-sm-3 invoice-col">
            <b>Invoice #{{ $data->code }}</b><br>
            <br>
            <b>Order ID:</b> 4F3S8J<br>
            <b>Payment Due:</b> 2/22/2014<br>
            <b>Account:</b> 968-34567
         </div>
         <!-- /.col -->
         <!-- /.col -->
         <div class="col-sm-3 invoice-col">
            @if ($data->payment_proof)
            <div>
               <p class="lead">Payment Proof</p>
               <img src="{{ asset('assets/img/payment/'. $data->payment_proof) }}" style="height: 150px; width: 340px; margin-left: -100px; margin-top: -20px;" data-toggle="modal" data-target="#modal-payment" class="btn">
            </div>
            {{-- <div style="text-align: center;">
               <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#modal-payment" style="margin-top: -17px;">
                <i class="fa fa-eye"></i> Preview Proof
               </button>
            </div> --}}
            @endif
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row" style="margin-top: 25px;">
         <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
               <thead>
                  <tr>
                     <th>Qty</th>
                     <th>Product</th>
                     <th>Subtotal</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td>1</td>
                     <td>{{ $data->product->name }}</td>
                     <td>IDR {{ number_format($data->price, 0, ',', '.') }}</td>
                  </tr>
               </tbody>
            </table>
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- this row will not appear when printing -->
      <div class="row no-print">
         <div class="col-xs-12">
            {{-- @if ($data->status == 'BOOKED')
               <a href="{{ url('admin/reservation/'. $data->id .'/confirm') }}" onclick="return confirm('Confirm..?');" class="btn btn-success pull-right"><i class="fa fa-check"></i> Confirm </a>
            @endif --}}
            @if ($data->status == 'PAID')
               @if(Auth::user()->role == 'super user')
                  <div class="col-md-6">
                     <a href="{{ url('admin/reservation/'. $data->id .'/print') }}" onclick="return confirm('Print..?');" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                  </div>
                  <div class="col-md-6">
                  {!! Form::open(['url' => url('admin/reservation/'. $data->id .'/complete'), 'role' => 'form', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
                     <input type="hidden" name="q" value="chat">
                     <button type="submit" class="btn btn-success pull-right" style="margin-top: 10px;" onclick="return confirm('Mark as complete..?');"><i class="fa fa-credit-card"></i> Complete</button>
                  {!! Form::close() !!}
                  </div>
               @else
                  <a href="{{ url('admin/reservation/'. $data->id .'/print') }}" onclick="return confirm('Print..?');" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
               @endif
            @endif
            @if ($data->status == 'BOOKED')
            <div class="row">
               <div class="col-md-6">
                  <a href="{{ url('admin/reservation/'. $data->id .'/print') }}" onclick="return confirm('Print..?');" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
               </div>
               <div class="col-md-6">
                  {!! Form::open(['url' => url('admin/reservation/'. $data->id .'/pay'), 'role' => 'form', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
                  {{-- <a href="{{ url('admin/reservation/'. $data->id .'/pay') }}" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit</a> --}}
                  <input type="hidden" name="q" value="pay">
									<div class="row">
										<div class="col-md-12">
						          @if (Session::has('message'))
						            {{-- Alert --}}
						            <div class="alert alert-{{ Session::get('type') }} alert-dismissible">
						              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						              <h4><i class="icon fa fa-{{ Session::get('icon') }}"></i> Success!</h4>
						              {{ Session::get('message') }}
						            </div>
						            {{-- Alert --}}
						          @endif
						          <div class="box box-success {{ Session::has('error') ? '' : 'collapsed-box' }}">
						            <div class="box-header with-border">
						              <h3 class="box-title">Add Chat & Payment Proof</h3>
						              <div class="box-tools pull-right">
						                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
						              </div>
						            </div>
						            <!-- /.box-header -->
						            <div class="box-body">
						              <div class="row">
						                <div class="col-md-2">

						                </div>
						                <div class="col-md-8">
						                  @if($errors->any())
						                    <div class="alert alert-danger">
						                      <ul>
						                        @foreach ($errors->all() as $error)
						                          <li>{{ $error }}</li>
						                        @endforeach
						                      </ul>
						                    </div>
						                  @endif
						                  <!-- form start -->
						                  <div class="box-body">
																<div class="form-group">
						                      <label for="image_payment" class="col-sm-2 control-label">Payment</label>
						                      <div class="col-sm-6">
						                        <input type="file" id="image_payment" name="image_payment" onchange="previewImage();" required>
						                      </div>
						                    </div>
						                    <div class="form-group">
						                      <label for="image_chat" class="col-sm-2 control-label">Chat</label>
						                      <div class="col-sm-6">
						                        <input type="file" id="image_chat" name="image_chat[]" onchange="previewImage();" required>
						                      </div>
																	<div class="col-sm-2">
																		<button type="button" name="button" id="btn-plus" class="btn btn-success pull-left"><i class="fa fa-plus"></i></button>
																	</div>
						                    </div>
																<div id="additional">

																</div>
							                  <button type="submit" class="btn btn-success pull-right" style="margin-top: 10px;" onclick="return confirm('Mark as paid..?');"><i class="fa fa-credit-card"></i> Submit & Confirm</button>
						                    {{-- <div class="form-group" style="text-align: center; vertical-align: middle; display: none;" id="preview-container">
						                      <label for="image" class="col-sm-2 control-label">Chat Proof Preview</label>
						                      <div class="col-sm-8">
						                        <img id="image-preview" src="{{ asset('assets/dist/img/blank.jpg') }}"  style="max-width: 400px; max-height: 300px;" alt="">
						                      </div>
						                    </div> --}}
						                  </div>
						                </div>
						              </div>
						              <!-- /.row -->
						            </div>
						          </div>
						        </div>
									</div>
                  {!! Form::close() !!}
               </div>
            </div>
            @endif
            @if (($data->status == 'COMPLETED') && (Auth::user()->role == 'super user' || Auth::user()->role == 'admin'))
               <a href="{{ url('admin/reservation/'. $data->id .'/print') }}" onclick="return confirm('Print..?');" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
               {{-- <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#modal-payment">
                <i class="fa fa-dollar"></i> Payment Proof
               </button> --}}
               <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#modal-chat" style="margin-right: 5px;">
                <i class="fa fa-commenting-o"></i> Chat Proof
               </button>
               {{-- <a href="{{ asset('assets/img/chat/'. $data->chat_proof) }}" target="_blank" class="btn btn-success pull-right" style="margin-right: 5px;"><i class="fa fa-commenting-o"></i> Chat Proof </a> --}}
            @endif
            {{-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
               <i class="fa fa-download"></i> Generate PDF
            </button> --}}
         </div>
      </div>
   </section>
   <div class="modal fade" id="modal-payment">
      <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header" style="text-align: center;">
             {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span></button> --}}
             <h4 class="modal-title">Payment Proof</h4>
           </div>
           <div class="modal-body">
             <img src="{{ asset('assets/img/payment/'. $data->payment_proof) }}" style="width: 570px;">
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
           </div>
         </div>
         <!-- /.modal-content -->
      </div>
    <!-- /.modal-dialog -->
   </div>
  <!-- /.modal -->
  <div class="modal fade" id="modal-chat">
      <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header" style="text-align: center;">
             {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span></button> --}}
             <h4 class="modal-title">Chat Proof</h4>
           </div>
           <div class="modal-body">
             @if ($data->chat_proof)
               @foreach ($data->chat_proof as $chats)
                 <img src="{{ asset('assets/img/chat/'. $chats) }}" style="width: 570px;">
               @endforeach
             @endif
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
           </div>
         </div>
         <!-- /.modal-content -->
      </div>
    <!-- /.modal-dialog -->
   </div>
  <!-- /.modal -->
   <!-- /.content -->
   <div class="clearfix"></div>
</div>
<!-- /.content-wrapper -->

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
   <script>
      $(document).ready(function(){
         console.log('document ready');

         $('#reservation').addClass('active');

				 var i = 1;
				 var plus = $('#btn-plus');
				 var container = $('#additional');

				 plus.click( function(){
					 console.log('button plus clicked');
					 if (i >= 4) {
					 	alert('Maximum 5 proofs..');
					 }
					 i++;
					 container.append('<div class="form-group" id="more'+ i +'">' +
						 '<label for="image_chat" class="col-sm-2 control-label">Chat</label>' +
						 '<div class="col-sm-6">' +
							 '<input type="file" id="image_chat" name="image_chat[]" onchange="previewImage();" required>' +
						 '</div>' +
						 '<div class="col-sm-2">' +
							 '<button type="button" name="button" class="btn btn-danger pull-left" id="'+ i +'"><i class="fa fa-times"></i></button>' +
						 '</div>' +
					 '</div>');
				 });

				$(document).on('click', '.btn-danger', function(){
					i--;
					var id = $(this).attr('id');

					$('#more'+ id).remove();
				});
      });
   </script>
@endpush

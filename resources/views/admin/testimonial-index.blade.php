@extends('admin.template')
@push('css')
   {{-- expr --}}
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('content')
   
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Testimonial
         <small>list</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
         <li class="active"> Testimonial</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-success {{ Session::has('error') ? '' : 'collapsed-box' }}">
               <div class="box-header with-border">
                  <h3 class="box-title">Add new</h3>

                  <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                     {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> --}}
                  </div>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <div class="row">
                     {{-- <div class="col-md-12" style="text-align: center; vertical-align: middle;">
                        <img id="image-preview" src="{{ asset('assets/dist/img/blank.jpg') }}"  style="max-width: 400px; max-height: 300px;" alt="">
                     </div> --}}
                     <div class="col-md-12">
                        @if($errors->any())
                           <div class="alert alert-danger">
                              <ul>
                                 @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                 @endforeach
                              </ul>
                           </div>
                        @endif
                        <!-- form start -->
                        {!! Form::open(['url' => url('admin/testimonial/'), 'role' => 'form', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
                           <div class="box-body">
                              <div class="form-group">
                                 <label for="title" class="col-sm-2 control-label">Name</label>
                                 <div class="col-sm-8">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name') }}" required>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="comment" class="col-sm-2 control-label">Comment</label>
                                 <div class="col-sm-8">
                                    <textarea class="form-control" rows="3" id="comment" name="comment" placeholder="Comment here" style="resize: none;" required>{{ old('comment') }}</textarea>
                                    <p class="help-block pull-right"><span id="chars">250</span> characters remainings.</p>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="from" class="col-sm-2 control-label">From</label>
                                 <div class="col-sm-8">
                                    <input type="text" class="form-control" id="from" name="from" placeholder="e.g Company/Country" value="{{ old('name') }}">
                                 </div>
                              </div>
                           </div>
                           <!-- /.box-body -->
                           <div class="box-footer">
                              <button type="button" class="btn btn-default" data-widget="collapse">Cancel</button>
                              <button type="submit" class="btn btn-success pull-right">Submit</button>
                           </div>
                           <!-- /.box-footer -->
                        {!! Form::close() !!}
                     </div>
                  </div>
                  <!-- /.row -->
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-xs-12">
            @if (Session::has('message'))
               {{-- Alert --}}
               <div class="alert alert-{{ Session::get('type') }} alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-{{ Session::get('icon') }}"></i> Success!</h4>
                  {{ Session::get('message') }}
               </div>
               {{-- Alert --}}
            @endif
            <div class="box box-success">
               <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th style="text-align: center;">Name</th>
                           <th>Comment</th>
                           <th style="width: 5%;">#</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($data as $d)
                           <tr>
                              <td align="center">
                                 {{ $d->name }} - <strong>{{ $d->from }}</strong><br>
                                 {{ \Carbon\Carbon::parse($d->created_at)->format('d/m/y') }}
                              </td>
                              <td>{{ $d->comment }}</td>
                              <td align="center">
                                 <div class="btn-group">
                                    {!! Form::open(['url' => url('admin/testimonial/'. $d->id), 'role' => 'form', 'method' => 'delete', 'class' => 'form-horizontal']) !!}
                                       {{-- <a href="{{ url('admin/testimonial/'. $d->id .'/edit') }}" class="btn btn-default"><i class="fa fa-pencil"></i></a> --}}
                                       <button type="submit" name="submit" class="btn btn-danger" onclick="return confirm('Delete data..?');"><i class="fa fa-trash"></i></button>
                                    {!! Form::close() !!}
                                 </div>
                              </td>
                           </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@push('plugin')
   {{-- expr --}}
   <!-- InputMask -->
   <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.js') }}"></script>
   <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
   <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
   <!-- DataTables -->
   <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
   <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
@endpush
@push('script')
   {{-- expr --}}
   <script>
      $(document).ready(function(){
         console.log('document ready');

         $('#testimonial').addClass('active');
         $('#example1').DataTable({
            'sort' : false
         });

         //Datemask dd/mm/yyyy
         $('#date').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });

         var max = 250;

         $('textarea').keyup( function(){
            var length = $(this).val().length;
            var length = max-length;
            $('#chars').text(length);

            if (length < 0) {
               $('.help-block').css('color', 'red');
               $(this).css('border-color', 'red');
            }else {
               $('.help-block').css('color', '');
               $(this).css('border-color', '');
            }
         });
      });

      function previewImage() {
         var reader = new FileReader();
         reader.readAsDataURL(document.getElementById("image").files[0]);

         reader.onload = function(readerEvent) {
         document.getElementById("image-preview").src = readerEvent.target.result;
         };
      };
   </script>
@endpush
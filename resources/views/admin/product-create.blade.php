@extends('admin.template')
@push('css')
	{{-- expr --}}
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endpush
@section('content')
	
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Product
         <small>Create</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
         <li><a href="{{ url('admin/product') }}"> Product</a></li>
         <li class="active"><a href="#"> Create</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- right column -->
         {!! Form::open(['url' => url('admin/product/'), 'role' => 'form', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
         <div class="col-md-12">
            @if($errors->any())
               <div class="alert alert-danger">
                  <ul>
                     @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                     @endforeach
                  </ul>
               </div>
            @endif
            <!-- Horizontal Form -->
            <div class="box box-success">
               <div class="box-header with-border">
                  <h3 class="box-title">Product Form</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <div class="box-body">
                  <div class="col-md-6" style="text-align: center; vertical-align: middle;">
                     <img id="image-preview" src="{{ asset('assets/dist/img/blank.jpg') }}"  style="max-width: 400px; max-height: 300px;" alt="">
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name') }}" required>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="desc" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                           <textarea class="form-control" rows="5" id="desc" name="desc" placeholder="Description here" style="resize: none;" required>{{ old('desc') }}</textarea>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="price" class="col-sm-2 control-label">Price</label>
                        <div class="col-sm-5">
                           <input type="number" class="form-control" id="price" name="price" placeholder="Price" value="{{ old('price') }}" required>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" class="form-control" id="unit" name="unit" placeholder="e.g Pax/Session/Day" value="{{ old('unit') }}" required>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="image" class="col-sm-2 control-label">Select Image</label>
                        <div class="col-sm-10">
                           <input type="file" id="image" name="image" onchange="previewImage();" required multiple>
                           <p class="help-block">Example block-level help text here.</p>
                        </div>
                     </div>
                     <div class="form-group">
                         <label for="is_promo" class="col-sm-2 control-label">Promo</label>
                         <div class="col-sm-5">
                           <select class="form-control" id="is_promo" name="is_promo">
                              <option value="0" selected>No</option>
                              <option value="1">Yes</option>
                           </select>
                        </div>
                     </div>
                     {{-- <div class="form-group">
                        <label for="category" class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control" id="category" name="category" placeholder="Category" value="{{ old('category') }}" required>
                        </div>
                     </div> --}}
                     <div class="form-group">
                         <label for="category" class="col-sm-2 control-label">Category</label>
                         <div class="col-sm-5">
                           <select class="form-control" id="category" name="category">
                              <option value="Aerial Photography">Aerial Photography</option>
                              <option value="Photography">Photography</option>
                              <option value="Printed Stuff">Printed Stuff</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="additional_info" class="col-sm-2 control-label">Additional Info</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control" id="additional_info" name="additional_info" placeholder="Additional Info (not required)" value="{{ old('additional_info') }}">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12" id="promo-section" style="display: none;">
                     <div class="box-header with-border">
                        <h3 class="box-title">Promotion Setting</h3>
                     </div>
                     <div class="box-body">
                        <div class="form-group">
                           <label for="promo_price" class="col-sm-2 control-label">Price</label>
                           <div class="col-sm-5">
                              <input type="number" class="form-control" id="promo_price" name="promo_price" placeholder="Price" value="{{ old('promo_price') }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="promo_start" class="col-sm-2 control-label">Date Start:</label>
                           <div class="input-group col-sm-9" style="padding-left: 15px;">
                              <div class="input-group-addon">
                                 <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control date-mask" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask="" style="width: 335px;" id="promo_start" name="promo_start">
                           </div>
                           <!-- /.input group -->
                        </div>
                        <div class="form-group">
                           <label for="promo_end" class="col-sm-2 control-label">Date End:</label>
                           <div class="input-group col-sm-9" style="padding-left: 15px;">
                              <div class="input-group-addon">
                                 <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control date-mask" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask="" style="width: 335px;" id="promo_end" name="promo_end">
                           </div>
                           <!-- /.input group -->
                        </div>
                        <div class="form-group">
                           <label for="promo_image" class="col-sm-2 control-label">Select Promo Image</label>
                           <div class="col-sm-10">
                              <input type="file" id="promo_image" name="promo_image" onchange="previewImage();" multiple>
                              <p class="help-block">Example block-level help text here.</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /.box-body -->
               <div class="box-footer">
                  <a href="{{ url('admin/product') }}" type="button" class="btn btn-default">Cancel</a>
                  <button type="submit" class="btn btn-success pull-right">Submit</button>
               </div>
               <!-- /.box-footer -->
            </div>
            <!-- /.box -->
         </div>
         {!! Form::close() !!}
         <!-- /.row -->
      </section>
      <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@push('plugin')
	{{-- expr --}}
   <!-- Select2 -->
   <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
   <!-- InputMask -->
   <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.js') }}"></script>
   <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
   <script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
   {{-- Additional --}}
   <script src="{{ url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/politespace.js') }}"></script>
@endpush
@push('script')
	{{-- expr --}}
   <script>
      $(document).ready(function(){
         console.log('document ready');

         $('#product').addClass('active');

         $('#is_promo').change( function(){
            console.log($(this).val());

            if ($(this).val() == 1) {
               $('#promo-section').css('display', 'block');
            }else{
               $('#promo-section').css('display', 'none');
            }
         });

         //Datemask dd/mm/yyyy
         $('.date-mask').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
      });

      function previewImage() {
         var reader = new FileReader();
         reader.readAsDataURL(document.getElementById("image").files[0]);

         reader.onload = function(readerEvent) {
         document.getElementById("image-preview").src = readerEvent.target.result;
         };
      }; 
   </script>
@endpush
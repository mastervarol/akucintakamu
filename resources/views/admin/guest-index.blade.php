@extends('admin.template')
@push('css')
	{{-- expr --}}
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('content')
	
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Guest
         <small>list</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('admin/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
         <li class="active"><a href="#"> Guest</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            @if (Session::has('message'))
               {{-- Alert --}}
               <div class="alert alert-{{ Session::get('type') }} alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-{{ Session::get('icon') }}"></i> Success!</h4>
                  {{ Session::get('message') }}
               </div>
               {{-- Alert --}}
            @endif
            <div class="box box-success">
               <!-- /.box-header -->
               {{-- <div class="box-header">
                  <a href="{{ url('admin/user/create') }}" class="btn btn-success" style="width: 50px; height: 25px;"><i class="fa fa-plus"></i></a>
               </div> --}}
               <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>Name</th>
                           <th>Email</th>
                           <th>Phone</th>
                           {{-- <th style="width: 10%;">#</th> --}}
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($data as $d)
                           <tr>
                              <td>{{ $d->name }}</td>
                              <td>{{ $d->email }}</td>
                              <td>{{ $d->phone ? $d->phone : '' }}</td>
                              {{-- <td>
                                 <div class="btn-group">
                                    {!! Form::open(['url' => url('admin/user/'. $d->id), 'role' => 'form', 'method' => 'delete', 'class' => 'form-horizontal']) !!}
                                       <a href="{{ url('admin/user/'. $d->id .'/edit') }}" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                                       <button type="submit" name="submit" class="btn btn-danger" onclick="return confirm('Delete data..?');"><i class="fa fa-trash"></i></button>
                                    {!! Form::close() !!}
                                 </div>
                              </td> --}}
                           </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@push('plugin')
	{{-- expr --}}
   <!-- DataTables -->
   <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
   <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
@endpush
@push('script')
	{{-- expr --}}
   <script>
      $(document).ready(function(){
         console.log('document ready');

         $('#guest').addClass('active');
         $('#example1').DataTable();
      });
   </script>
@endpush
@extends('template')
@push('css')
	{{-- expr --}}
	<style type="text/css">
		@import url(https://fonts.googleapis.com/css?family=Lato:700);
		.box {
		  position: relative;
		  max-width: 600px;
		  width: 90%;
		  height: 400px;
		  background: #fff;
		  box-shadow: 0 0 15px rgba(0,0,0,.1);
		}

		/* common */
		.ribbon {
		  width: 150px;
		  height: 150px;
		  overflow: hidden;
		  position: absolute;
		}
		.ribbon::before,
		.ribbon::after {
		  position: absolute;
		  z-index: -1;
		  content: '';
		  display: block;
		  border: 5px solid #2980b9;
		}
		.ribbon span {
		  position: absolute;
		  display: block;
		  width: 225px;
		  padding: 15px 0;
		  background-color: #111111;
		  box-shadow: 0 5px 10px rgba(0,0,0,.1);
		  color: #fff;
		  font: 700 18px/1 'Lato', sans-serif;
		  text-shadow: 0 1px 1px rgba(0,0,0,.2);
		  text-transform: uppercase;
		  text-align: center;
		  opacity: 0.8;
		}

		/* top left*/
		.ribbon-top-left {
		  top: 0px;
		  /*left: -10px;*/
		}
		.ribbon-top-left::before,
		.ribbon-top-left::after {
		  border-top-color: transparent;
		  border-left-color: transparent;
		}
		.ribbon-top-left::before {
		  top: 0;
		  right: 0;
		}
		.ribbon-top-left::after {
		  bottom: 0;
		  left: 0;
		}
		.ribbon-top-left span {
		  right: -25px;
		  top: 30px;
		  transform: rotate(-45deg);
		}

		/* top right*/
		.ribbon-top-right {
		  top: -10px;
		  right: -10px;
		}
		.ribbon-top-right::before,
		.ribbon-top-right::after {
		  border-top-color: transparent;
		  border-right-color: transparent;
		}
		.ribbon-top-right::before {
		  top: 0;
		  left: 0;
		}
		.ribbon-top-right::after {
		  bottom: 0;
		  right: 0;
		}
		.ribbon-top-right span {
		  left: -25px;
		  top: 30px;
		  transform: rotate(45deg);
		}

		/* bottom left*/
		.ribbon-bottom-left {
		  bottom: -10px;
		  left: -10px;
		}
		.ribbon-bottom-left::before,
		.ribbon-bottom-left::after {
		  border-bottom-color: transparent;
		  border-left-color: transparent;
		}
		.ribbon-bottom-left::before {
		  bottom: 0;
		  right: 0;
		}
		.ribbon-bottom-left::after {
		  top: 0;
		  left: 0;
		}
		.ribbon-bottom-left span {
		  right: -25px;
		  bottom: 30px;
		  transform: rotate(225deg);
		}

		/* bottom right*/
		.ribbon-bottom-right {
		  bottom: -10px;
		  right: -10px;
		}
		.ribbon-bottom-right::before,
		.ribbon-bottom-right::after {
		  border-bottom-color: transparent;
		  border-right-color: transparent;
		}
		.ribbon-bottom-right::before {
		  bottom: 0;
		  left: 0;
		}
		.ribbon-bottom-right::after {
		  top: 0;
		  right: 0;
		}
		.ribbon-bottom-right span {
		  left: -25px;
		  bottom: 30px;
		  transform: rotate(-225deg);
		}
		.page-link{
			border-color: #111111 !important;
			color: #111111 !important;
		}
		.pagination>.active>span {
			background-color: #111111 !important;
			color: #fff !important;
		}
	</style>
@endpush
@section('content')

<div id="main-content" class="edge-padding">
	
	<!-- page content -->
	<div class="page-section single-blog">
		<section class="single-blog-main">
			<div class="container">
				<div class="row">
					<div class="col-xlarge-8 col-medium-8">
						<!-- post image -->
						<img src="{{ asset('assets/img/product/'. $product->image) }}" alt="" class="single-image image" />
						@if ($product->is_promo == 1)
							<div class="ribbon ribbon-top-left"><span>PROMOOO!!!</span></div>
						@endif
						<!-- post title -->
						<div class="single-blog-title">
							<h2 class="font-reg">{{ $product->name }}</h2>
							<p class="font-reg">28 March 2016</p>
						</div>
						<!-- post content -->
						<div class="page-content single-blog-content">
							<p>{{ $product->description }}</p>
						</div>
						<!-- post share -->
						{{-- <div id="post-share" class="small-social clearfix">
							<a href="" class="social-item hov-bk" target="_blank" data-popup="facebook"><span class="fa fa-facebook"></span></a>
							<a href="" class="social-item hov-bk" target="_blank" data-popup="twitter"><span class="fa fa-twitter"></span></a>
							<a href="" class="social-item hov-bk" target="_blank" data-popup="googlePlus"><span class="fa fa-google-plus"></span></a>
							<a href="" class="social-item hov-bk" target="_blank" data-popup="pinterest"><span class="fa fa-pinterest"></span></a>
							<a href="" class="social-item hov-bk" target="_blank" data-popup="stumbleupon"><span class="fa fa-stumbleupon"></span></a>
							<a href="" class="social-item hov-bk" target="_blank" data-popup="linkedin"><span class="fa fa-linkedin"></span></a>
						</div> --}}
					</div>
				
					<!-- post sidebar -->
					<div class="col-xlarge-4 col-medium-4 post-sidebar right-sidebar">
						<!-- list widget example -->
						<div class="sidebar-widget list-widget">
							<h3 class="font-reg">Information</h3>
							<ul class="font-reg">
								@if ($product->is_promo == 1)
									<li><a href="">
										Price : <del><strong>{{ number_format($product->price, 0, ',', '.') }}</strong></del>
										<strong>{{ number_format($product->promo_price, 0, ',', '.') }}</strong>
									</a></li>
									<li><a href="">Date : <strong>{{ \Carbon\Carbon::parse($product->promo_start)->format('d M Y') }} - {{ \Carbon\Carbon::parse($product->promo_end)->format('d M Y') }}</strong></a></li>
								@else
									<li><a href="">Price : <strong>{{ number_format($product->price, 0, ',', '.') }}</strong></a></li>
								@endif
								<li><a href="">Per : <strong>{{ $product->unit }}</strong></a></li>
								<li><a href="">Category : <strong>{{ $product->category }}</strong></a></li>
							</ul>
						</div>
						<!-- social widget -->
						<div class="sidebar-widget social-widget">
							<h3 class="font-reg">Follow</h3>
							<ul>
								<li><a href="{{ \App\Option::find(1)->facebook_link }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<li><a href="{{ \App\Option::find(1)->twitter_link }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<li><a href="{{ \App\Option::find(1)->instagram_link }}" target="_blank"><i class="fa fa-instagram"></i></a></li>
							</ul>
						</div>
						@if ($product->additional_info != '')
							<!-- text widget -->
							<div class="sidebar-widget font-reg social-widget">
								<h3 class="font-reg">Additional Info</h3>
								<div class="textwidget">{{ $product->additional_info }}</div>
							</div>
						@endif
						<!-- text widget -->
						<div class="sidebar-widget font-reg social-widget">
							<h3 class="font-reg">Make a Reservation</h3>
							<div style="text-align: center;">
								<a href="{{ url('reservation/') }}" class="primary-button font-reg hov-bk">BOOK NOW</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
@endpush
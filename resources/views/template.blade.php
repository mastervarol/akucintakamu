<!DOCTYPE html>
<html lang="en">
<head>
	<!-- page title -->
	<title>{{ \App\Option::find(1)->site_title }} - {{ \App\Option::find(1)->site_tagline }}</title>

	<!-- theme css --> 
	<link href="{{ asset('assets/css/structure.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet" type="text/css" />
	<!-- carousel css -->
	<link href="{{ asset('assets/js/lib/owl-carousel/owl.carousel.css') }}" rel="stylesheet">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">

	<!-- author -->
	<meta name="author" content="mastervarol.com">
	<!-- responsive meta tag -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta charset="UTF-8">

	<!-- google fonts used -->
	<link href='{{ url('https://fonts.googleapis.com/css?family=Lato:400,300') }}' rel='stylesheet' type='text/css'>
	<link href='{{ url('https://fonts.googleapis.com/css?family=Open+Sans') }}' rel='stylesheet' type='text/css'>

	<!-- whatsapp button -->
	<style type="text/css">
		.float{
			position:fixed;
			width:60px;
			height:60px;
			bottom:40px;
			right:40px;
			background-color:#111111;
			color:#FFF;
			border-radius:50px;
			text-align:center;
		  font-size:30px;
			box-shadow: 2px 2px 3px #999;
		  z-index:100;
		}

		.my-float{
			margin-top:16px;
		}
	</style>

	<!-- additional css -->
	@stack('css')
</head>
<body>

<!-- header -->
<header>
	<div class="header-main clearfix edge-padding header-narrow">
		<div class="header-main-inside">
			<div class="container clearfix">
				<div class="medium-header-container">
					<!-- Site logo -->
					<a href="{{ url('/') }}" id="site-logo"></a>
					<!-- Mobile burger icon -->
					<div id="mobile-nav-button">
						<div id="mobile-nav-icon">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>
				</div>
				<!-- header nav menu -->
				<nav id="header-nav">
					<ul id="nav-ul" class="menu font-reg clearfix">
						<!-- Nav item - with two tier drop down -->
						<li class="menu-item menu-item-has-children">
							<a href="{{ url('/') }}">Home<span class="sub-drop-icon fa fa-angle-down"></span></a>
						</li>
						<!-- Nav item - with two tier drop down -->
						<li class="menu-item menu-item-has-children">
							<a href="{{ url('price-promo') }}">Price & Promo<span class="sub-drop-icon fa fa-angle-down"></span></a>
						</li>
						<!-- Nav item - with two tier drop down -->
						<li class="menu-item menu-item-has-children">
							<a href="{{ url('gallery') }}">Gallery<span class="sub-drop-icon fa fa-angle-down"></span></a>
						</li>
						<!-- Nav item - with two tier drop down -->
						<li class="menu-item menu-item-has-children">
							<a href="{{ url('reservation') }}">Reservation<span class="sub-drop-icon fa fa-angle-down"></span></a>
						</li>
						<!-- Nav item - with two tier drop down -->
						<li class="menu-item menu-item-has-children">
							<a href="#">About<span class="sub-drop-icon fa fa-angle-down"></span></a>
							<ul class="sub-menu sub-menu-first">
								<li><a href="{{ url('contact-us') }}">Contact Us</a></li>
								<li class="menu-item menu-item-has-children">
									<a href="{{ url('term-condition') }}">Terms and Conditions<span class="sub-drop-icon sub-second-drop fa fa-angle-down"></span></a>
								</li>
							</ul>
						</li>
						<!-- Nav item - with two tier drop down -->
						<li class="menu-item menu-item-has-children">
							<a href="{{ url('/blog') }}">Our Blog<span class="sub-drop-icon fa fa-angle-down"></span></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>

@yield('content')

<!-- footer -->
<footer>
	<div class="footer-main edge-padding">
		<div class="footer-main-inside">
			<div class="container clearfix">
				<!-- footer top content -->
				<div class="footer-top">
					<div class="row">
						<!-- footer about text section -->
						<div class="col-xlarge-4 col-medium-12">
							<div class="footer-about-section">
								<h4 class="font-reg">About</h4>
								<div class="page-content">
									<p>{{ \App\Option::find(1)->about }}</p>
								</div>
							</div>
						</div>
						@if (count(\App\Post::All()) > 0)
							<!-- footer latest blog posts section -->
							<div class="col-xlarge-4 col-medium-6">
								<div class="footer-posts-section">
									<h4 class="font-reg">Latest posts</h4>
									<div class="posts-widget">
										<ul>
											@foreach (\App\Post::orderBy('created_at')->limit(2)->get() as $element)
											<!-- item example 1 -->
											<li>
												<a href="{{ url('blog/'. $element->title) }}">
													<div class="row">
														<div class="col-xlarge-5 col-medium-4 col-small-5">
															<img src="{{ asset('assets/img/blog/'. $element->image) }}" alt="" class="image" style="height: 75px;"/>
														</div>
														<div class="col-xlarge-7 col-medium-8 col-small-7">
															<h4 class="font-reg">{{ $element->title }}</h4>
															<p>{{ \Carbon\Carbon::parse($element->created_at)->format('d M Y') }}</p>
														</div>
													</div>
												</a>
											</li>
											@endforeach
										</ul>
									</div>
								</div>
							</div>
						@endif
						@if (count(\App\Gallery::All()) > 0)
							<!-- footer latest work posts section -->
							<div class="col-xlarge-4 col-medium-6">
								<div class="footer-posts-section">
									<h4 class="font-reg">Latest work</h4>
									<div class="posts-widget">
										<ul>
											@foreach (\App\Gallery::orderBy('created_at')->limit(2)->get() as $element)
											<!-- item example 1 -->
											<li>
												<a href="{{ url('gallery/'. $element->name) }}">
													<div class="row">
														<div class="col-xlarge-5 col-medium-4 col-small-5">
															<img src="{{ asset('assets/img/gallery/'. $element->image) }}" alt="" class="image" style="height: 75px;"/>
														</div>
														<div class="col-xlarge-7 col-medium-8 col-small-7">
															<h4 class="font-reg">{{ $element->name }}</h4>
															<p>{{ \Carbon\Carbon::parse($element->created_at)->format('d M Y') }}</p>
														</div>
													</div>
												</a>
											</li>
											@endforeach
										</ul>
									</div>
								</div>
							</div>
						@endif
					</div>
				</div>
				@if(\App\Option::find(1)->whatsapp != '')
					<a href="https://api.whatsapp.com/send?phone={{ \App\Option::find(1)->whatsapp }}&text={{ \App\Option::find(1)->whatsapp_text }}" class="float" target="_blank">
					<i class="fa fa-whatsapp my-float"></i>
					</a>
				@endif
				<!-- footer bottom content -->
				<div class="footer-bottom clearfix">
					<!-- footer copyright text -->
					<p class="font-reg">© {{ \Carbon\Carbon::now()->format('Y') }} Instinct. <a href="http://www.mastervarol.com" target="_blank">Created by mastervarol.</a></p>
					<!-- footer social icons -->
					<ul class="footer-social">
						<li><a href="{{ \App\Option::find(1)->facebook_link }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li><a href="{{ \App\Option::find(1)->twitter_link }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<li><a href="{{ \App\Option::find(1)->instagram_link }}" target="_blank"><i class="fa fa-instagram"></i></a></li>
					</ul>
					<!-- scroll to top -->
					<div id="scroll-top">
						<span class="fa fa-angle-up"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<script type="text/javascript" src="{{ asset('assets/js/jquery-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/owl-carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/isotope/isotope.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/isotope/packery.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/main.js') }}"></script>
<!-- additional plugin -->
@stack('plugin')
<!-- additional script -->
@stack('script')
</body>
</html>
@extends('template')
@push('css')
	{{-- expr --}}
@endpush
@section('content')

<div id="main-content" class="edge-padding">
	<!-- Portfolio section -->
	<section class="portfolio-section page-section">
		<div class="container">
			<!-- Portfolio filter -->
			<ul id="portfolio-item-filter" class="font-reg clearfix">
				<li><a href="#" class="active" data-filter="*">All</a></li>
				@foreach (\App\Gallery::groupBy('category')->get() as $element)
					<li><a href="#" class="active" data-filter=".{{ $element->category }}">{{ $element->category }}</a></li>
				@endforeach
			</ul>
			<!-- portfolio item list -->
			<ul id="portfolio-items" class="row clearfix">
				<!-- portfolio item example 1 -->
				@foreach ($gallery as $element)
				<li class="col-xlarge-3 col-medium-3 {{ $element->category }}">
					<a href="{{ url('gallery/'. $element->name) }}"  class="portfolio-item">
						<img src="{{ asset('assets/img/gallery/'. $element->image) }}" class="image" alt="" style="height: 225px;" />
						<!-- portfolio item hover -->
						<div class="portfolio-hover">
							<div class="portfolio-hover-content font-reg">
								<h3>{{ $element->name }}</h3>
								<p>{{ $element->category }}</p>
							</div>
						</div>
					</a>
				</li>
				@endforeach
			</ul>
		</div>
	</section>
</div>

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
@endpush

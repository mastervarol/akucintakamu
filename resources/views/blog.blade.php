@extends('template')
@push('css')
	{{-- expr --}}
@endpush
@section('content')

<div id="main-content" class="edge-padding">
	<section class="blog-section page-section">
		<div class="container">
			<div class="row">
				<div class="col-xlarge-12">
					<ul class="blog-list row">
						@foreach ($post as $element)
							<!-- Wide blog item example -->
							<li class="col-xlarge-4 col-medium-6">
								<div class="blog-item wide-blog-item">
									<a href="{{ url('blog/'. $element->title) }}"><img src="{{ asset('assets/img/blog/'. $element->image) }}" alt="" class="image" style="height: 225px;" /></a>
									<h3 class="font-reg"><a href="">{{ $element->title }}</a></h3>
									<div class="blog-item-meta">
										<span>{{ \Carbon\Carbon::parse($element->created_at)->format('d M Y') }}</span>
									</div>
									<div class="page-content">
										<p>{{ $element->short_desc }}</p>
									</div>
									<a href="{{ url('blog/'. $element->title) }}" class="primary-button font-reg hov-bk">Read more</a>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
				<div class="col-xlarge-12">
					{{ $post->links() }}
				</div>
			</div>
		</div>
	</section>
</div>

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
@endpush
@extends('template')
@push('css')
	{{-- expr --}}
	<style type="text/css">
		@import url(https://fonts.googleapis.com/css?family=Lato:700);
		.box {
		  position: relative;
		  max-width: 600px;
		  width: 90%;
		  height: 400px;
		  background: #fff;
		  box-shadow: 0 0 15px rgba(0,0,0,.1);
		}

		/* common */
		.ribbon {
		  width: 150px;
		  height: 150px;
		  overflow: hidden;
		  position: absolute;
		}
		.ribbon::before,
		.ribbon::after {
		  position: absolute;
		  z-index: -1;
		  content: '';
		  display: block;
		  border: 5px solid #2980b9;
		}
		.ribbon span {
		  position: absolute;
		  display: block;
		  width: 225px;
		  padding: 15px 0;
		  background-color: #111111;
		  box-shadow: 0 5px 10px rgba(0,0,0,.1);
		  color: #fff;
		  font: 700 18px/1 'Lato', sans-serif;
		  text-shadow: 0 1px 1px rgba(0,0,0,.2);
		  text-transform: uppercase;
		  text-align: center;
		  opacity: 0.8;
		}

		/* top left*/
		.ribbon-top-left {
		  top: 0px;
		  /*left: -10px;*/
		}
		.ribbon-top-left::before,
		.ribbon-top-left::after {
		  border-top-color: transparent;
		  border-left-color: transparent;
		}
		.ribbon-top-left::before {
		  top: 0;
		  right: 0;
		}
		.ribbon-top-left::after {
		  bottom: 0;
		  left: 0;
		}
		.ribbon-top-left span {
		  right: -25px;
		  top: 30px;
		  transform: rotate(-45deg);
		}

		/* top right*/
		.ribbon-top-right {
		  top: -10px;
		  right: -10px;
		}
		.ribbon-top-right::before,
		.ribbon-top-right::after {
		  border-top-color: transparent;
		  border-right-color: transparent;
		}
		.ribbon-top-right::before {
		  top: 0;
		  left: 0;
		}
		.ribbon-top-right::after {
		  bottom: 0;
		  right: 0;
		}
		.ribbon-top-right span {
		  left: -25px;
		  top: 30px;
		  transform: rotate(45deg);
		}

		/* bottom left*/
		.ribbon-bottom-left {
		  bottom: -10px;
		  left: -10px;
		}
		.ribbon-bottom-left::before,
		.ribbon-bottom-left::after {
		  border-bottom-color: transparent;
		  border-left-color: transparent;
		}
		.ribbon-bottom-left::before {
		  bottom: 0;
		  right: 0;
		}
		.ribbon-bottom-left::after {
		  top: 0;
		  left: 0;
		}
		.ribbon-bottom-left span {
		  right: -25px;
		  bottom: 30px;
		  transform: rotate(225deg);
		}

		/* bottom right*/
		.ribbon-bottom-right {
		  bottom: -10px;
		  right: -10px;
		}
		.ribbon-bottom-right::before,
		.ribbon-bottom-right::after {
		  border-bottom-color: transparent;
		  border-right-color: transparent;
		}
		.ribbon-bottom-right::before {
		  bottom: 0;
		  left: 0;
		}
		.ribbon-bottom-right::after {
		  top: 0;
		  right: 0;
		}
		.ribbon-bottom-right span {
		  left: -25px;
		  bottom: 30px;
		  transform: rotate(-225deg);
		}
		.page-link{
			border-color: #111111 !important;
			color: #111111 !important;
		}
		.pagination>.active>span {
			background-color: #111111 !important;
			color: #fff !important;
		}
	</style>
@endpush
@section('content')

<div id="main-content" class="edge-padding">
	<!-- featured section -->
	<section class="featured-section featured-wide">
		<div id="featured-slideshow-outer" class="carousel-outer">
			<!-- Previous slide button -->
			<span class="slideshow-btn previous-slide-btn fa fa-angle-left"></span>
			<!-- featured slideshow -->
			<div id="featured-slideshow" class="carousel" data-autoplay="false" data-autoplay-speed="5000" data-animation-in="fadeIn" data-animation-out="fadeOut">
				<!-- featured slide example -->
				@foreach ($slide as $element)
				<div class="featured-slide" style="background-image:url('{{ asset('assets/img/slide/'. $element->image) }}');">
					<div class="container">
						<div class="featured-slide-content font-reg">
							<h2>{{ $element->title }}</h2>
							<p>{{ $element->content }}</p>
							<a href="{{ $element->link }}" class="primary-button font-reg hov-bk">Read more</a>
						</div>
					</div>
				</div>
				@endforeach
			</div>
			<!-- Next slide button -->
			<span class="slideshow-btn next-slide-btn fa fa-angle-right"></span>
		</div>
	</section>
	<section class="block-section">
		<div class="container">
			<div class="row">
				<!-- block example -->
				<div class="col-xlarge-4 col-medium-4">
					<a href="{{ url('price-promo?c=Aerial Photography') }}" class="block-item">
						<img src="assets/img/block_placeholder_1.jpg" alt="" class="image">
						<div class="block-item-overlay"></div>
						<div class="block-item-inside font-reg">
							<h3>Aerial Photography</h3>
						</div>
					</a>
				</div>
				<!-- block example -->
				<div class="col-xlarge-4 col-medium-4">
					<a href="{{ url('price-promo?c=Photography') }}" class="block-item">
						<img src="assets/img/block_placeholder_2.jpg" alt="" class="image">
						<div class="block-item-overlay"></div>
						<div class="block-item-inside font-reg">
							<h3>Photography</h3>
						</div>
					</a>
				</div>
				<!-- block example -->
				<div class="col-xlarge-4 col-medium-4">
					<a href="{{ url('price-promo?c=Printed Stuff') }}" class="block-item">
						<img src="assets/img/block_placeholder_3.jpg" alt="" class="image">
						<div class="block-item-overlay"></div>
						<div class="block-item-inside font-reg">
							<h3>Printed Stuff</h3>
						</div>
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="blog-section page-section">
		<div class="container">
			<div class="row">
				<div class="col-xlarge-1 col-medium-1">
				</div>
				<div class="col-xlarge-9 col-medium-9">
					<ul class="blog-list">
						<!-- Wide blog item example -->
						<li>
							<div class="blog-item wide-blog-item">
								<div class="page-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque nibh enim, quis euismod enim lacinia nec. Phasellus quam diam, semper in erat eu, efficitur molestie purus. Sed a elementum mi. Sed interdum mattis risus, sit amet eleifend ligula luctus ut. Sed ullamcorper lorem aliquam, tincidunt lorem et, ultrices est. Suspendisse eleifend dui odio, id volutpat quam iaculis eu. Nunc sit amet scelerisque mauris. Phasellus volutpat mauris ac sem tincidunt, in fringilla arcu ultrices. Phasellus scelerisque eros vel pulvinar gravida. Aenean suscipit felis orci, sed egestas libero dignissim at. Sed commodo malesuada ligula, nec vehicula risus fermentum sed.</p>
								</div>
							</div>
						</li>
						@foreach ($product as $element)
							<!-- Small blog item example -->
							<li>
								<div class="blog-item">
									<div class="row">
										<div class="col-xlarge-5 col-medium-4">
											<a href="{{ url('product/'. $element->name) }}"><img src="{{ asset('assets/img/product/'. $element->image ) }}" alt="" class="image" style="height: 180px;" />
											@if ($element->is_promo == 1)
												<div class="ribbon ribbon-top-left"><span>PROMOOO!!!</span></div>
											@endif
											</a>
										</div>
										<div class="col-xlarge-7 col-medium-8">
											<h3 class="font-reg"><a href="">{{ $element->name }}</a></h3>
											<div class="blog-item-meta">
												<span>{{ \Carbon\Carbon::parse($element->created_at)->format('d M Y') }}</span>
											</div>
											<div class="page-content">
												<p>{{ $element->description }}</p>
											</div>
											<a href="{{ url('product/'. $element->name) }}" class="primary-button font-reg hov-bk">Read more</a>
										</div>
									</div>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
				<div class="col-xlarge-1 col-medium-1">
				</div>
			</div>
			@if (isset(request()->c))
				<div class="row">
					<div class="col-md-12" style="text-align: center; margin-top: 50px; margin-bottom: 50px;">
						<a href="{{ url('price-promo') }}" class="primary-button font-reg hov-bk">Show All</a>
					</div>
				</div>
			@endif
			<div class="row" style="text-align: center; margin-bottom: -60px;">
         	{{ $product->links() }}
      	</div>
		</div>
	</section>
</div>

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
@endpush
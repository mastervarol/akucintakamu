@extends('template')
@push('css')
	{{-- expr --}}
@endpush
@section('content')

<div id="main-content" class="edge-padding">
	<!-- featured section -->
	<section class="featured-section featured-wide">
		<div id="featured-slideshow-outer" class="carousel-outer">
			<!-- Previous slide button -->
			<span class="slideshow-btn previous-slide-btn fa fa-angle-left"></span>
			<!-- featured slideshow -->
			<div id="featured-slideshow" class="carousel" data-autoplay="true" data-autoplay-speed="5000" data-animation-in="fadeIn" data-animation-out="fadeOut">
			@foreach ($slide as $element)
				<!-- featured slide example -->
				<div class="featured-slide" style="background-image:url('{{ asset('assets/img/slide/'. $element->image) }}');">
					<div class="container">
						<div class="featured-slide-content font-reg">
							<h2>{{ $element->title }}</h2>
							<p>{{ $element->content }}</p>
							<a href="{{ $element->link }}" target="_blank" class="primary-button font-reg hov-bk">Read more</a>
						</div>
					</div>
				</div>
			@endforeach
			</div>
			<!-- Next slide button -->
			<span class="slideshow-btn next-slide-btn fa fa-angle-right"></span>
		</div>
	</section>
	<!-- Services section -->
	<section class="services-section page-section">
		<div class="container">
			<div class="row">
				<!-- Service item example -->
				<div class="col-xlarge-4 col-medium-4">
					<div class="service-item">
						<i class="fa fa-pencil"></i>
						<h3 class="font-reg">Aerial Photography</h3>
						<p>Duis iaculis tellus vitae ante consequat. Maecenas ut molestie est. Sed vitae tincidunt elit.</p>
					</div>
				</div>
				<!-- Service item example -->
				<div class="col-xlarge-4 col-medium-4">
					<div class="service-item">
						<i class="fa fa-briefcase"></i>
						<h3 class="font-reg">Photography</h3>
						<p>Duis iaculis tellus vitae ante consequat. Maecenas ut molestie est. Sed vitae tincidunt elit.</p>
					</div>
				</div>
				<!-- Service item example -->
				<div class="col-xlarge-4 col-medium-4">
					<div class="service-item">
						<i class="fa fa-pencil-square-o"></i>
						<h3 class="font-reg">Photobook & Printed Stuff</h3>
						<p>Duis iaculis tellus vitae ante consequat. Maecenas ut molestie est. Sed vitae tincidunt elit.</p>
					</div>
				</div>
				<!-- Service item example -->
				<div class="col-xlarge-4 col-medium-4">
					<div class="service-item">
						<i class="fa fa-life-ring"></i>
						<h3 class="font-reg">Design</h3>
						<p>Duis iaculis tellus vitae ante consequat. Maecenas ut molestie est. Sed vitae tincidunt elit.</p>
					</div>
				</div>
				<!-- Service item example -->
				<div class="col-xlarge-4 col-medium-4">
					<div class="service-item">
						<i class="fa fa-area-chart"></i>
						<h3 class="font-reg">Website</h3>
						<p>Duis iaculis tellus vitae ante consequat. Maecenas ut molestie est. Sed vitae tincidunt elit.</p>
					</div>
				</div>
				<!-- Service item example -->
				{{-- <div class="col-xlarge-4 col-medium-4">
					<div class="service-item">
						<i class="fa fa-cloud"></i>
						<h3 class="font-reg">Modern technology</h3>
						<p>Duis iaculis tellus vitae ante consequat. Maecenas ut molestie est. Sed vitae tincidunt elit.</p>
					</div>
				</div> --}}
			</div>
		</div>
	</section>
	<!-- Portfolio section -->
	<section class="portfolio-section page-section section-border">
		<div class="container">
			<!-- Portfolio filter -->
			<ul id="portfolio-item-filter" class="font-reg clearfix">
				<li><a href="{{ url('/') }}" class="active" data-filter="*">All</a></li>
				@foreach (\App\Gallery::groupBy('category')->get() as $element)
					<li><a href="#" class="active" data-filter=".{{ $element->category }}">{{ $element->category }}</a></li>
				@endforeach
			</ul>
			<!-- portfolio item list -->
			<ul id="portfolio-items" class="row clearfix">
				<!-- portfolio item example 1 -->
				@foreach ($gallery as $element)
				<li class="col-xlarge-3 col-medium-3 {{ $element->category }}">
					<a href="{{ url('gallery/'. $element->name) }}"  class="portfolio-item">
						<img src="{{ asset('assets/img/gallery/'. $element->image) }}" class="image" alt="" style="height: 225px;" />
						<!-- portfolio item hover -->
						<div class="portfolio-hover">
							<div class="portfolio-hover-content font-reg">
								<h3>{{ $element->name }}</h3>
								<p>{{ $element->category }}</p>
							</div>
						</div>
					</a>
				</li>
				@endforeach
			</ul>
		</div>
	</section>
	<!-- Testimonial section -->
	<section class="testimonial-section page-section section-border">
		<div class="container">
			<!-- Section title -->
			<div class="section-title font-reg">
				<h2>Client testimonials</h2>
			</div>
			<!-- Testimonial slideshow -->
			<div id="testimonial-slideshow" class="carousel" data-autoplay="true" data-autoplay-speed="5000">
				@foreach ($testimonial as $element)
					<!-- Testimonial slide example -->
					<div class="testimonial-slide font-reg">
						<p>"{{ $element->comment }}"</p>
						<p>{{ $element->name }} / <span class="company">{{ $element->from }}</span></p>
					</div>
				@endforeach
			</div>
		</div>
	</section>
</div>

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
@endpush

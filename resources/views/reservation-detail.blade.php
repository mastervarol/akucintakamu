@extends('template')
@push('css')
	{{-- expr --}}
@endpush
@section('content')

<div id="main-content">
	<div class="feature-page">
		<!-- theme slideshows -->
		<section class="feature-slideshows feature-section">
			<div class="container">
				<!-- Section title -->
				<div class="section-title font-reg">
					<h2>Reservation </h2>
					<p>Create stunning slideshow to showcase images</p>
				</div>
				<div id="feature-slideshow-outer" class="carousel-outer">
					<!-- Previous slide button -->
					<span class="slideshow-btn previous-slide-btn fa fa-angle-left"></span>
					<!-- Single portfolio slideshow -->
					<div id="feature-slideshow" class="carousel">
						@foreach ($slide as $element)
							<!-- feature slide example -->
							<div class="feature-slide" style="background-image:url('{{ asset('assets/img/slide/'. $element->image) }}');">
							</div>
						@endforeach
					</div>
					<!-- Next slide button -->
					<span class="slideshow-btn next-slide-btn fa fa-angle-right"></span>
				</div>
				@if(Session::has('message'))
					<div class="row">
						<div class="col-xlarge-12 col-medium-12 col-small-12">
							<div id="success-message" class="notification" style="display: block; text-align: center; "><p class="font-reg"></p>{{ Session::get('message') }}</div>
						</div>
						<div class="col-xlarge-6 col-medium-6 col-small-6">
							<div class="column-item">
								<p>Product selected : {{ $product->name }}</p>
								<input type="hidden" name="product" value="{{ $product->id }}">
							</div>
						</div>
						<div class="col-xlarge-6 col-medium-6 col-small-6">
							<div class="column-item">
								<p>On date : {{ \Carbon\Carbon::parse($selected_date)->format('d M Y') }}</p>
								<input type="hidden" name="date" value="{{ $product->id }}">
							</div>
						</div>
					</div>
					<div class="row" style="margin-top: 20px;">
						<div class="col-xlarge-4 col-medium-4 col-small-4">
							<div class="column-item">
								<p>Guest name : {{ Session::get('guest_name') }}</p>
							</div>
						</div>
						<div class="col-xlarge-4 col-medium-4 col-small-4">
							<div class="column-item">
								<p>Guest email : {{ Session::get('guest_email') }}</p>
							</div>
						</div>
						<div class="col-xlarge-4 col-medium-4 col-small-4">
							<div class="column-item">
								<p>Guest Phone : {{ Session::get('guest_phone') }}</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xlarge-12 col-medium-12 col-small-12" style="text-align: center; margin-top: 50px;">
							<a href="{{ url('reservation') }}" class="primary-button font-reg hov-bk">BOOK MORE</a>
						</div>
					</div>
				@else
					<div class="row">
						<div class="col-xlarge-6 col-medium-6 col-small-6">
							<div class="column-item">
								<p>Product selected : {{ $product->name }}</p>
							</div>
						</div>
						<div class="col-xlarge-6 col-medium-6 col-small-6">
							<div class="column-item">
								<p>On date : {{ \Carbon\Carbon::parse($selected_date)->format('d M Y') }}</p>
							</div>
						</div>
					</div>
					{!! Form::open(['url' => url('reservation/new/'. $product->id), 'role' => 'form', 'method' => 'POST']) !!}
					<div class="row" style="margin-top: 25px;">
						<input type="hidden" name="product" value="{{ $product->id }}">
						<input type="hidden" name="date" value="{{ $selected_date }}">
						<div class="col-xlarge-4 col-medium-4 col-small-4">
							<input type="text" class="contact-input font-reg" name="contact_name" id="contact_name" value="" placeholder="Your Name" tabindex="1" required>
						</div>
						<div class="col-xlarge-4 col-medium-4 col-small-4">
							<input type="email" class="contact-input font-reg" name="contact_email" id="contact_email" value="" placeholder="Your Email" tabindex="2" required>
						</div>
						<div class="col-xlarge-4 col-medium-4 col-small-4">
							<input type="text" class="contact-input font-reg" name="contact_phone" id="contact_phone" value="" placeholder="Your Phone" tabindex="3">
						</div>
					</div>
					<input type="submit" class="primary-button font-reg hov-bk pull-right" value="BOOK NOW">
					{{-- <a class="book primary-button font-reg hov-bk pull-right" target="_blank">BOOK NOW</a> --}}
					{!! Form::close() !!}
				@endif
			</div>
		</section>
	</div>

</div>

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
	<script type="text/javascript">
		$(document).ready( function(){
			var book = $('.book');

			book.click( function(){
				var name = $('#contact_name').val();
				var email = $('#contact_email').val();
				var phone = $('#contact_phone').val();

				console.log(name);

				$(this).attr('href', 'https://api.whatsapp.com/send?phone={{ \App\Option::find(1)->whatsapp }}&text=Booking:{{ $product->name }} / Date: {{ \Carbon\Carbon::parse($selected_date)->format('d M Y') }} / Name:' + name + ' / Email:' + email);
			});
		});
	</script>
@endpush

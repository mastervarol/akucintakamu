@extends('template')
@push('css')
	{{-- expr --}}
@endpush
@section('content')

<div id="main-content" class="edge-padding">
	<section class="blog-section page-section">
		<div class="container">
			<div class="row">
				<!-- page side spacing -->
				<div class="col-xlarge-1"></div>
				<div class="col-xlarge-10">
					<!-- post image -->
					<img src="{{ asset('assets/img/blog/'. $post->image) }}" alt="" class="single-image image" />
					<!-- post title -->
					<div class="single-blog-title">
						<h2 class="font-reg">{{ $post->title }}</h2>
						<p class="font-reg">{{ \Carbon\Carbon::parse($post->created_at)->format('d M Y') }}</p>
					</div>
					<!-- post content -->
					<div class="page-content single-blog-content">
						<p>{{ html_entity_decode($post->long_desc) }}</p>
					</div>
					<!-- post share -->
					<div id="post-share" class="small-social clearfix">
						<a href="{{ \App\Option::find(1)->facebook_link }}" class="social-item hov-bk" target="_blank" data-popup="facebook"><span class="fa fa-facebook"></span></a>
						<a href="{{ \App\Option::find(1)->twitter_link }}" class="social-item hov-bk" target="_blank" data-popup="twitter"><span class="fa fa-twitter"></span></a>
						<a href="{{ \App\Option::find(1)->instagram_link }}" class="social-item hov-bk" target="_blank" data-popup="instagram"><span class="fa fa-instagram"></span></a>
					</div>
				</div>
				<div class="row">
					<div class="col-xlarge-12" style="text-align: center;">
						<a href="{{ url('blog') }}" class="primary-button font-reg hov-bk" style="margin-top: 50px;">More post</a>
					</div>
				</div>
				<!-- page side spacing -->
				<div class="col-xlarge-1"></div>
			</div>
		</div>
	</section>
</div>

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
@endpush
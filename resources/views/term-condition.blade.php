@extends('template')
@push('css')
	{{-- expr --}}
@endpush
@section('content')

<div id="main-content" class="edge-padding">
	<!-- page head section -->
	<section class="page-header" style="background-image:url('assets/img/header_placeholder_1.jpg');">
		<div class="page-head-inside">
			<div class="container">
				<h1 class="font-reg">Term & Conditions</h1>
				<p class="font-reg">Sed pellentesque nibh enim quis</p>
			</div>
		</div>
	</section>
	<!-- page content -->
	<div class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-xlarge-1 col-medium-1">
				</div>
				<div class="col-xlarge-12 col-medium-12">
					<!-- standard page content -->
					<div class="page-content">
						<p>{{ $option->term_condition }}</p>
					</div>
				</div>
				<div class="col-xlarge-1 col-medium-1">
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
@endpush
@extends('template')
@push('css')
	{{-- expr --}}
@endpush
@section('content')

<div id="main-content">
	<!-- page content -->
	<section class="page-section single-portfolio">
		<div class="container">
			<div id="post-slideshow-outer" class="carousel-outer">
				<!-- Previous slide button -->
				<span class="slideshow-btn previous-slide-btn fa fa-angle-left"></span>
				<!-- Single portfolio slideshow -->
				<div id="post-slideshow" class="carousel" data-autoplay="false" data-autoplay-speed="5000" data-animation-in="fadeIn" data-animation-out="fadeOut">
					<!-- slide example 1 -->
					<div class="post-slide">
						<img src="{{ asset('assets/img/gallery/'. $gallery->image) }}" class="image" alt="" />
					</div>
				</div>
				<!-- Next slide button -->
				<span class="slideshow-btn next-slide-btn fa fa-angle-right"></span>
			</div>
			<div class="row">
				<div class="col-xlarge-9 single-portfolio-left">
					<h1 class="font-reg">{{ $gallery->name }}</h1>
					<div class="page-content">
						<p>{{ $gallery->description }}</p>
					</div>
					<!-- post share -->
					<div id="post-share" class="small-social clearfix">
						<a href="{{ \App\Option::find(1)->facebook_link }}" class="social-item hov-bk" target="_blank" data-popup="facebook"><span class="fa fa-facebook"></span></a>
						<a href="{{ \App\Option::find(1)->twitter_link }}" class="social-item hov-bk" target="_blank" data-popup="twitter"><span class="fa fa-twitter"></span></a>
						<a href="{{ \App\Option::find(1)->instagram_link }}" class="social-item hov-bk" target="_blank" data-popup="instagram"><span class="fa fa-instagram"></span></a>
					</div>
				</div>
				<div class="col-xlarge-3">
					<div class="detail-block">
						<h3 class="font-reg">Category</h3>
						<span>{{ $gallery->category }}</span>
					</div>
					<div class="detail-block">
						<h3 class="font-reg">Date</h3>
						<span>{{ \Carbon\Carbon::parse($gallery->date)->format('d M Y') }}</span>
					</div>
					<div class="detail-block">
						<h3 class="font-reg">Link</h3>
						<a href="http://www.lucid-themes.com">{{ $gallery->link }}</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xlarge-12" style="text-align: center;">
					<a href="{{ url('gallery') }}" class="primary-button font-reg hov-bk">More gallery</a>
				</div>
			</div>
			<!-- post navigation -->
			{{-- <section class="post-navigation">
				<div id="post-nav-main" class="clearfix">
					<a href="" id="post-nav-prev" class="post-nav-item">
						<p class="font-reg"><i class="fa fa-long-arrow-left"></i>Previous Project</p>
					</a>
					<a href="" id="post-nav-next" class="post-nav-item">
						<p class="font-reg">Next Project<i class="fa fa-long-arrow-right"></i></p>
					</a>
				</div>
			</section> --}}
		</div>
	</section>
</div>

@endsection
@push('plugin')
	{{-- expr --}}
@endpush
@push('script')
	{{-- expr --}}
	<script type="text/javascript">
		$(document).ready(function(){
			$('.slideshow-btn').css('display', 'none');
		});
	</script>
@endpush
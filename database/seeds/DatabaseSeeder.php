<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->delete();

        DB::table('users')->insert([
        	'name' => strtoupper('super user'),
        	'email' => 'super@akucintakamu.com',
        	'password' => bcrypt('super'),
            'role' =>'super user'
        ]);

        DB::table('users')->insert([
            'name' => strtoupper('admin'),
            'email' => 'admin@akucintakamu.com',
            'password' => bcrypt('admin'),
            'role' =>'admin'
        ]);

        DB::table('users')->insert([
            'name' => strtoupper('reservation'),
            'email' => 'rsv@akucintakamu.com',
            'password' => bcrypt('reservation'),
            'role' =>'reservation'
        ]);

        DB::table('options')->insert([
            'about' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
            'term_condition' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.'
        ]);
    }
}

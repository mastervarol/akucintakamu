<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->text('about')->nullable();
            $table->string('site_title', 15)->nullable()->default('AKUCINTAKAMU');
            $table->string('site_tagline', 20)->nullable()->default('Blog & Portfolio');
            $table->string('facebook_link', 50)->nullable()->default('https://www.facebook.com');
            $table->string('twitter_link', 50)->nullable()->default('https://www.twitter.com');
            $table->string('instagram_link', 50)->nullable()->default('https://www.instagram.com');
            $table->string('email', 75)->nullable()->default('info@akucintakamu.com');
            $table->string('address', 100)->nullable()->default('Jl. Cintah');
            $table->string('phone', 20)->nullable()->default('0821');
            $table->string('whatsapp', 20)->nullable()->default('0821');
            $table->string('whatsapp_text', 75)->nullable()->default('Question please, ');
            $table->text('contact_text')->nullable();
            $table->text('term_condition')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');
    }
}

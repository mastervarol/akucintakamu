<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 75);
            $table->string('image', 75);
            $table->text('description');
            $table->decimal('price', 20, 2)->nullable()->default(0);
            $table->string('unit');
            $table->integer('is_promo')->nullable()->default(0)->comment('0 = False, 1 = True');
                $table->decimal('promo_price', 20, 2)->nullable()->default(0);
                $table->date('promo_start')->nullable();
                $table->date('promo_end')->nullable();
                $table->string('promo_image', 75)->nullable();
            $table->string('category', 75)->comment('e.g. Aerial Photography');
            $table->string('additional_info')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

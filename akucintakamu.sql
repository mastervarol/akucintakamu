-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Agu 2019 pada 12.06
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `akucintakamu`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `galleries`
--

CREATE TABLE `galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT 'uncategorized',
  `date` date NOT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '#',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `image`, `description`, `category`, `date`, `link`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Jimbaran Fish Market', '1e7a1b12972dcad63cf96daa55d35b68.jpg', 'Jimbaran dikenal dengan wisata kulinernya yang menawarkan beragam hidangan laut nan segar.', 'Photography', '2019-04-14', 'akucintakamu.id', '2019-08-13 22:08:14', '2019-08-16 01:22:35', '2019-08-16 01:22:35'),
(3, 'Fire Meets Food', 'ba37479371b2aeb816cb2d41388f997f.jpg', 'Aroma masakan yang dibakar memang berbeda, terlebih jika bahan masakannya masih segar. Nikamati hidangan seafood tersegar di Bali hanya di pantai Jimbaran. Aroma bumbu khas tiap resto akan memanjakan hidung anda begitu memasuki areal tempat berkumpulnya resto seafood lezat ini.\r\n\r\nCapture by: Angga Astayana\r\nTools:  SONY A7 II - Fix Lens', 'Photography', '2019-04-14', 'akucintakamu.id', '2019-08-16 01:20:58', '2019-08-16 01:20:58', NULL),
(4, 'Fresh Seafood', 'd4cedf925829e856cf7317155f8eeb93.jpg', 'Santapan laut nan segar menantikan anda di Jimbaran fine dining resto. Disini, anda dapat memilih sendiri bahan makanan yang akan anda pesan, semuanya rata-rata masih dalam keadaan hidup, sehingga kesegarannya terjamin. Setelah memilih jenis seafood yang ingin anda pesan, anda cukup menunggu di meja anda dan makanan pun akan dihidangkan. Lengkapi wisata kuliner anda dengan es kelapa muda utuh yang disajikan bersama dengan sirup gula yang manis bertemu segarnya asam jeruk nipis. \r\n\r\nCaptured by: Angga Astayana\r\nTools: SONY A7 II - Fix Lens', 'Photography', '2019-04-14', 'akucintakamu.id', '2019-08-16 01:26:54', '2019-08-16 01:26:54', NULL),
(5, 'Best Diner', '7405e5b3e4fb5b3b460d0afc6a2fadaa.jpg', 'Menikmati suasana makan malam yang tenang ditemani suara deburan ombak di pantai Jimbaran Bali memang sebuah kenikmatan tersendiri. Pesona pantai dengan gemerlap lilin memancarkan pesona yang indah tiada tara. Rasakan makanan anda menjadi 10x lipat lebih lezat ditemani pemandangan eksotis nan menenangkan ini.', 'Photography', '2019-04-14', 'akucintakamu.id', '2019-08-16 01:27:37', '2019-08-16 01:27:37', NULL),
(6, 'Party Overnight', '4e92bf286b0023f5542358e9b7ec5b03.jpg', 'Panjer Festival 2019 yang diselenggarakan di kelurahan Panjer, Denpasar dimeriahkan dengan penampilan sejumlah band ternama dari Bali, termasuk salah satunya adalah Lolot. Grup band lawas ini turut memeriahkan panggung festival ini. Cahaya lampu panggung yang berkeliaran melewati wajah para penonton yang antusias menyaksikan performa band-band ini menambah meriah suasana festival terbesar di Panjer ini.', 'Photography', '2019-05-14', 'akucintakamu.id', '2019-08-16 01:33:32', '2019-08-16 01:33:32', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `guests`
--

CREATE TABLE `guests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `guests`
--

INSERT INTO `guests` (`id`, `name`, `email`, `phone`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Dita', 'ditawordpress@gmail.com', '087777721432', '2019-08-13 08:04:37', '2019-08-13 08:04:37', NULL),
(2, 'Ayu', 'ayu@gmail.com', '087777721432', '2019-08-13 20:21:54', '2019-08-13 20:21:54', NULL),
(3, 'Bidadari', 'dari@gmail.com', '087777721432', '2019-08-13 20:54:22', '2019-08-13 20:54:22', NULL),
(4, 'Angga', 'angga@gmail.com', '087777721432', '2019-08-14 01:11:16', '2019-08-14 01:11:16', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `inquiries`
--

CREATE TABLE `inquiries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_read` int(11) DEFAULT 0 COMMENT '0 = Unread, 1 = Read',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(91, '2014_10_12_000000_create_users_table', 1),
(92, '2019_07_17_063244_create_guests_table', 1),
(93, '2019_07_17_064652_create_reservations_table', 1),
(94, '2019_07_17_064947_create_galleries_table', 1),
(95, '2019_07_17_065430_create_products_table', 1),
(96, '2019_07_17_131919_create_slides_table', 1),
(97, '2019_07_17_132612_create_testimonials_table', 1),
(98, '2019_07_17_132847_create_options_table', 1),
(99, '2019_07_17_133433_create_posts_table', 1),
(100, '2019_07_17_135221_create_inquiries_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `options`
--

CREATE TABLE `options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_title` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT 'AKUCINTAKAMU',
  `site_tagline` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'Blog & Portfolio',
  `facebook_link` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'https://www.facebook.com',
  `twitter_link` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'https://www.twitter.com',
  `instagram_link` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'https://www.instagram.com',
  `email` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT 'info@akucintakamu.com',
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'Jl. Cintah',
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '0821',
  `whatsapp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '0821',
  `whatsapp_text` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT 'Question please, ',
  `contact_text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `term_condition` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `options`
--

INSERT INTO `options` (`id`, `about`, `site_title`, `site_tagline`, `facebook_link`, `twitter_link`, `instagram_link`, `email`, `address`, `phone`, `whatsapp`, `whatsapp_text`, `contact_text`, `term_condition`) VALUES
(1, 'Apa saya yang kami kerjakan di AKUCINTAKAMU Photography? Kami menyediakan jasa Aerial Photography (pemotretan dari udara) &amp; jasa fotografi untuk event, pra-wedding, pemotretan keluarga, trip &amp; travelling, foto produk, makanan dan lainnya. Kami juga menyediakan jasa desain untuk segala kebutuhan dan website untuk melengkapi kebutuhan bisnis anda.', 'AKUCINTAKAMU', 'Photography', 'https://www.facebook.com', 'https://www.twitter.com', 'https://www.instagram.com', 'info@akucintakamu.com', 'Jl. Tukad Irawadi Gg.4 No.2', '081949494946', '6281712011045', 'Question please,', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_desc` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `long_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'uncategorized',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `posts`
--

INSERT INTO `posts` (`id`, `image`, `title`, `short_desc`, `long_desc`, `category`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '318de40fe379f6e9aa6d78f94e6e4cd9.jpg', 'Jimbaran Fish Market, Seafood Paradise', 'Jimbaran dikenal dengan wisata kulinernya yang menawarkan beragam hidangan laut nan segar. Kemudahan akses dan sensasi makan dipinggir pantai membuat acara santap malam anda akan semakin berkesan. Nikmati suasana pantai malam sembari mendengarkan deburan ombak, ditemani cahaya lilin yang redup memanjakan mata. Beberapa grup musik freelance pun turut meramaikan suasana santap malam, cukup membayar 50 ribu rupiah, serangkai lagu akan dinyanyikan khusus untuk anda.', '&lt;p&gt;&lt;/p&gt;\r\n&lt;div&gt;Bali identik dengan keromantisan serta penuh dengan energi yang \r\nmembuat kota ini selalu ramai dikunjungi oleh turis luar maupun dari \r\ndalam negeri. Bukan hanya ditunjuk sebagai salah satu destinasi bulan \r\nmadu bagi setiap pengantin baru, Bali juga ditunjuk sebagai tempat \r\nberlibur bersama keluarga. &lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;Bali terbagi \r\nmenjadi beberapa wilayah yang terkenal atau yang pasti dipenuhi oleh \r\nturis. Untuk belanja tentunya turis-turis akan pergi ke Kuta-Legian, \r\nkarena disana terdapat butik-butik serta kios-kios dengan harga untuk \r\nkalangan menengah kebawah. Dan Untuk seminyak, lebih untuk kalangan \r\nmenengah keatas.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;Dan untuk soal makanan dan \r\nsuasana, setiap orang yang pergi ke Bali, pasti tidak akan melewatkan \r\nmakan malam di Jimbaran. Di Jimbaran, terdapat beberapa restaurant \r\nseafood yang berada di pinggir laut. Seakan-akan Anda hanya ditemani \r\noleh lilin dan bintang-bintang di langit, serta suara ombak sebagai \r\nlatar belakang Anda. Bukan hanya itu, Bagi Anda yang ingin suasana lebih\r\n romantis, tersedia sekawanan band akustik yang akan mengiringi makan \r\nmalam Anda.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;Bagi Anda pecinta makanan laut, \r\njangan lupa untuk minum kelapa karena kelapa dipercaya dapat \r\nmenetralisir racun/ mengurangi kolestrol yang diakibatkan oleh seafood \r\nyang Anda konsumsi.&lt;/div&gt;\r\n\r\n&lt;p&gt;&lt;/p&gt;', 'Traveller Photography', '2019-08-13 22:07:29', '2019-08-16 01:46:13', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(20,2) DEFAULT 0.00,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_promo` int(11) DEFAULT 0 COMMENT '0 = False, 1 = True',
  `promo_price` decimal(20,2) DEFAULT 0.00,
  `promo_start` date DEFAULT NULL,
  `promo_end` date DEFAULT NULL,
  `promo_image` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'e.g. Aerial Photography',
  `additional_info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `description`, `price`, `unit`, `is_promo`, `promo_price`, `promo_start`, `promo_end`, `promo_image`, `category`, `additional_info`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Aerial Photography By DJI Phantom 4', '87fab401dc11d5a2bd73eb2fa981acc5.jpg', 'Aerial Photography using DJI Phantom 4.', '700000.00', '1 Battery Life (cc)', 0, '0.00', NULL, NULL, NULL, 'Aerial Photography', '* Tidak termasuk biaya transportasi/ area Denpasar transportasi gratis', '2019-08-13 08:03:40', '2019-08-16 01:47:10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `reservations`
--

CREATE TABLE `reservations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `price` decimal(20,2) NOT NULL,
  `status` enum('BOOKED','CONFIRMED','COMPLETED','PAID') COLLATE utf8mb4_unicode_ci DEFAULT 'BOOKED',
  `payment_proof` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chat_proof` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `reservations`
--

INSERT INTO `reservations` (`id`, `product_id`, `guest_id`, `code`, `date`, `price`, `status`, `payment_proof`, `chat_proof`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 1, 1, 'GNuLMdd', '2019-09-12', '700000.00', 'PAID', '4ca22888bc3b297162156550719b6e84.jpg', 'ad98c5cf85a58efeaecd73b94e52ac37.jpg', '2019-08-13 21:00:50', '2019-08-14 00:50:02', NULL),
(5, 1, 4, 'XbBczUz', '2019-12-01', '700000.00', 'BOOKED', NULL, '049c60a001e5154bd7b97d51ed11e077.jpg', '2019-08-14 01:11:16', '2019-08-14 01:11:16', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `slides`
--

CREATE TABLE `slides` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'reservation',
  `page` enum('HOME','RESERVATION','PRICE & PROMO') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `slides`
--

INSERT INTO `slides` (`id`, `image`, `title`, `content`, `link`, `page`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '8206cf8cd61cc83adfbec60b4f12eb2e.jpg', 'Welcome To AKUCINTAKAMU Photography', 'Kami menyediakan jasa aerial photography untuk berbagai kebutuhan anda. Nikmati foto kebersamaan dan momen bahagia anda dari ketinggian hingga lebih dari 150meter.', 'http://localhost/akucintakamu/public/price-promo?c=Aerial%20Photography', 'HOME', '2019-08-13 21:14:24', '2019-08-13 21:14:24', NULL),
(2, 'f3f691581fc607bff58325d12ceb5e25.jpg', 'Another Turquoise Sea View In Bali', 'Pesona pantai Melasti yang masih asri kini dapat kita nikmati dari ketinggian. Kunjungi blog kami untuk mengetahui spot-spot foto terbaik di Bali.', 'http://localhost/akucintakamu/public/blog', 'HOME', '2019-08-13 21:16:06', '2019-08-13 21:57:44', '2019-08-13 21:57:44'),
(3, 'd0841168433b8066b58154b89a7015c0.jpg', 'Melasti Beach, Bali', 'Pemandangan eksotis yang hanya dapat dinikmati di ketinggian 150m dari permukaan laut. Dapatkan penawaran foto udara terbaik dari kami sekarang juga.', 'http://localhost/akucintakamu/public/price-promo?c=Aerial%20Photography', 'HOME', '2019-08-13 21:58:23', '2019-08-13 21:58:23', NULL),
(4, '973c6ad70125bd237f0d30a6b61f79e1.jpg', 'Jendela Raksasa Di Tengah Laut', 'Dari mana datangnya jendela sebesar ini? Jangan terkecoh dengan foto yang kami tangkap dari ketinggian 100m ini ya. Cek cerita lengkapnya di blog kami!', 'http://localhost/akucintakamu/public/blog', 'HOME', '2019-08-13 21:58:48', '2019-08-13 21:58:48', NULL),
(5, '4f98697df94a53f438412b1c817877d0.jpg', 'Tasku Cantik, Incomeku Cantik', 'Tips meningkatkan pemasukan dengan foto produk yang berkualitas! Nikmati promo khusus dari kami khusus bagi anda pemilik online shop yang ingin meningkatkan penjualan anda dengan foto produk! Baca tips lengkapnya di blog kami.', 'http://localhost/akucintakamu/public/price-promo?c=Photography', 'HOME', '2019-08-13 21:59:23', '2019-08-13 21:59:23', NULL),
(6, 'be8611e6e1ad910c452d3be95de5cbdb.jpg', 'Price & Promo', 'Nikmati beragam promo menarik dari kami. Dapatkan layanan foto terbaik untuk beragam kebutuhan anda.', 'http://localhost/akucintakamu/public/price-promo?c=Aerial%20Photography', 'PRICE & PROMO', '2019-08-16 01:50:52', '2019-08-16 01:50:52', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `comment` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('super user','admin','reservation') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SUPER USER', 'super@akucintakamu.com', NULL, '$2y$10$tNZbQDZnW3RiqWmAn01qy.ecjYLTmUHfQicTxpHX.c7K3CuPrGVIa', 'super user', NULL, NULL, NULL, NULL),
(2, 'ADMIN', 'admin@akucintakamu.com', NULL, '$2y$10$DQk7/tptCbDvFQEBNAREMuar0UVPmLe/nKOSSGDCSD7j.XDeZirqG', 'admin', NULL, NULL, NULL, NULL),
(3, 'RESERVATION', 'rsv@akucintakamu.com', NULL, '$2y$10$GuVBgXxZ36Ml8Ak4vr93/.Cg3T47LKffPAfALMLHdNpOyKnD7LiRq', 'reservation', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `guests`
--
ALTER TABLE `guests`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `inquiries`
--
ALTER TABLE `inquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `guests`
--
ALTER TABLE `guests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `inquiries`
--
ALTER TABLE `inquiries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT untuk tabel `options`
--
ALTER TABLE `options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `slides`
--
ALTER TABLE `slides`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

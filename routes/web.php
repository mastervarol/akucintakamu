<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Frontend
Route::get('login', 'LoginController@index')->name('login');
	Route::post('login', 'LoginController@doLogin');
Route::get('/', 'FoController@getHome');
Route::get('/reservation', 'FoController@getReservation');
	Route::get('/reservation/detail/{product}', 'FoController@getReservationDetail');
	Route::post('/reservation/new/{product}', 'FoController@newReservation');
Route::get('/gallery', 'FoController@getGallery');
	Route::get('/gallery/{title}', 'FoController@getGalleryDetail');
Route::get('/price-promo', 'FoController@getPricePromo');
	Route::get('/product/{name}', 'FoController@getProduct');
Route::get('/contact-us', 'FoController@getContact');
	Route::post('/contact-us/send', 'FoController@sendMessage');
Route::get('/term-condition', 'FoController@getTermCondition');
Route::get('/blog', 'FoController@getPost');
	Route::get('/blog/{title}', 'FoController@getPostDetail');

 // Bacnkend
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){
	Route::get('/', 'DashboardController@index');
	Route::resource('user', 'UserController');
	Route::resource('gallery', 'GalleryController');
	Route::resource('guest', 'GuestController');
	Route::resource('inquiry', 'InquiryController');
	Route::resource('blog', 'PostController');
	Route::resource('product', 'ProductController');
	Route::resource('reservation', 'ReservationController');
		Route::get('reservation/{id}/confirm', 'ReservationController@confirm');
		Route::post('reservation/{id}/complete', 'ReservationController@complete');
		Route::post('reservation/{id}/pay', 'ReservationController@pay');
		Route::get('reservation/{id}/print', 'ReservationController@print');
	Route::resource('slide', 'SlideController');
	Route::resource('testimonial', 'TestimonialController');

	Route::resource('option', 'OptionController');
	// Logout
	Route::post('logout', 'LoginController@doLogout')->name('logout');
});
